pluginManagement {
    repositories {
        google {
            content {
                includeGroupByRegex("com\\.android.*")
                includeGroupByRegex("com\\.google.*")
                includeGroupByRegex("androidx.*")
            }
        }
        mavenCentral()
        gradlePluginPortal()
    }
}
dependencyResolutionManagement {
    repositoriesMode.set(RepositoriesMode.FAIL_ON_PROJECT_REPOS)
    repositories {
        maven {
            url = uri("https://api.mapbox.com/downloads/v2/releases/maven")
            credentials.username = "mapbox"
            credentials.password = "sk.eyJ1IjoiN3VucG9uIiwiYSI6ImNsdHpqZHQ0bDAwcGcybm85dWg3Nnh5OGoifQ._EFd7GExv8HZ8ed27CMBcQ"
            authentication.create<BasicAuthentication>("basic")
        }
        google()
        mavenCentral()
    }
}

rootProject.name = "EventApp"
include(":app")

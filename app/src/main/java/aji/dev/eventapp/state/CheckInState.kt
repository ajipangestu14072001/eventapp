package aji.dev.eventapp.state

import aji.dev.eventapp.model.Scan
import androidx.camera.view.PreviewView

data class CheckInState(
    val previewView: PreviewView? = null,
    val scan: Scan? = null,
    val showBottomSheet: Boolean = false,
    val showCameraRequiredDialog: Boolean = false
)
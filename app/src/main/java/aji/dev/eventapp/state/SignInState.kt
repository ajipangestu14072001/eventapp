package aji.dev.eventapp.state

data class SignInState(
    val isSignInSuccessful: Boolean = false,
    val signInError: String? = null
)
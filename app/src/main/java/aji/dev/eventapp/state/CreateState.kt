package aji.dev.eventapp.state

import aji.dev.eventapp.network.response.CreateResponse
import aji.dev.eventapp.network.response.UploadResponse
import androidx.compose.runtime.Immutable

@Immutable
data class CreateState(
    var isLoading: Boolean = true,
    val data: UploadResponse? = null,
    val create: CreateResponse? = null,
    val error: String? = null
) {
    companion object {
        val initialState: CreateState
            get() = CreateState()
    }
}
package aji.dev.eventapp.state

import aji.dev.eventapp.network.response.DetailResponse
import aji.dev.eventapp.network.response.EventResponse
import androidx.compose.runtime.Immutable

@Immutable
data class EventState(
    var isLoading: Boolean = true,
    val data: EventResponse? = null,
    val detail: DetailResponse? = null,
    val error: String? = null
) {
    companion object {
        val initialState: EventState
            get() = EventState()
    }
}
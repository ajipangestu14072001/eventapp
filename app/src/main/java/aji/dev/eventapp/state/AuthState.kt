package aji.dev.eventapp.state

import aji.dev.eventapp.network.response.AuthResponse
import androidx.compose.runtime.Immutable

@Immutable
data class AuthState(
    var isLoading: Boolean = true,
    val data: AuthResponse? = null,
    val error: String? = null
) {
    companion object {
        val initialState: AuthState
            get() = AuthState()
    }
}
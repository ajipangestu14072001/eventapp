package aji.dev.eventapp.network.response

import com.squareup.moshi.Json

data class EventLocation(

    @field:Json(name ="Type")
    var type: String? = null,
    @field:Json(name ="Lat")
    var lat: Double? = null,
    @field:Json(name ="Lng")
    var lng: Double? = null,
    @field:Json(name ="Address")
    var address: String? = null,
    @field:Json(name ="AddressFull")
    var addressFull: String? = null,
    @field:Json(name ="City")
    var city: String? = null,
    @field:Json(name ="Province")
    var province: String? = null,
    @field:Json(name ="Country")
    var country: String? = null

)
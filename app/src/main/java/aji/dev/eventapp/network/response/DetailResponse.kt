package aji.dev.eventapp.network.response

import com.squareup.moshi.Json

data class DetailResponse (

    @field:Json(name ="Results" )
    var results : EventDetail = EventDetail(),
    @field:Json(name ="Status"  )
    var status  : Status  = Status()

)

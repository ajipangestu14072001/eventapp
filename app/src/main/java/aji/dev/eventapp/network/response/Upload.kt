package aji.dev.eventapp.network.response

import com.squareup.moshi.Json

data class Upload(
    @field:Json(name ="category")
    var category: String? = "",
    @field:Json(name ="destination")
    var destination: String? = "",
    @field:Json(name ="filename")
    var filename: String? = "",
    @field:Json(name ="url")
    var url: String = ""
)

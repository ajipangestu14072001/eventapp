package aji.dev.eventapp.network.request

data class RegisterRequest(
    val email: String = "",
    val name: String = "",
    val password: String = "",
    val phone: String = ""
)
package aji.dev.eventapp.network.response

import com.squareup.moshi.Json

data class EventListPresented(
    @field:Json(name = "Avatar")
    val avatar: String,
    @field:Json(name = "Name")
    val name: String
)
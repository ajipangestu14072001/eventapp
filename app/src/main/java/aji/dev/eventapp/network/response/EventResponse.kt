package aji.dev.eventapp.network.response

import com.squareup.moshi.Json

data class EventResponse(

    @field:Json(name = "Results")
    var results: List<Event> = emptyList(),
    @field:Json(name = "Status")
    var status: Status? = Status()

)
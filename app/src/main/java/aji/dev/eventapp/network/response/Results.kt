package aji.dev.eventapp.network.response

import com.squareup.moshi.Json


data class Results(
    @field:Json(name = "Authorize")
    val authorize: Boolean,
    @field:Json(name = "Message")
    val message: String,
    @field:Json(name = "Role")
    val role: String,
    @field:Json(name = "Token")
    val token: String
)
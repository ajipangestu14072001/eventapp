package aji.dev.eventapp.network.response

import com.squareup.moshi.Json

data class EventTicketInfo(

    @field:Json(name = "BelongingToUserName")
    var belongingToUserName: String? = null,
    @field:Json(name = "BelongingToUserEmail")
    var belongingToUserEmail: String? = null,
    @field:Json(name = "IsYour")
    var isYour: Boolean? = null,
    @field:Json(name = "StatusTicket")
    var statusTicket: String? = null,
    @field:Json(name = "IsSoldOut")
    var isSoldOut: Boolean? = null

)
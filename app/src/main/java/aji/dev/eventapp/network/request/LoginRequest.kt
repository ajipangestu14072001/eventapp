package aji.dev.eventapp.network.request

data class LoginRequest(
    val email_phone: String = "",
    val password: String = ""
)
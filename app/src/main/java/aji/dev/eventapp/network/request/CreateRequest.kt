package aji.dev.eventapp.network.request

data class CreateRequest(
    val style : String = "pattern",
    val pattern : String = "style3",
    val background: String = "#290808",
    val date: Date,
    val description: String,
    val email: String,
    val location: Location,
    val phone: String,
    val price: List<Price>,
    val registration_questions: List<RegistrationQuestion>,
    val require_approval: Boolean,
    val text: String = "white",
    val thumbnail: String,
    val ticket: List<Ticket>,
    val title: String,
    val visibility: Boolean
)
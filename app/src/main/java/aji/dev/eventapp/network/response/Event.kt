package aji.dev.eventapp.network.response

import com.squareup.moshi.Json

data class Event(
    @field:Json(name = "Background")
    val background: String,
    @field:Json(name = "Code")
    val code: String,
    @field:Json(name = "EventListDate")
    val eventListDate: EventListDate,
    @field:Json(name = "EventListLocation")
    val eventListLocation: EventListLocation,
    @field:Json(name = "EventListPresented")
    val eventListPresented: EventListPresented,
    @field:Json(name = "ID")
    val id: Int,
    @field:Json(name = "Text")
    val text: String,
    @field:Json(name = "Thumbnail")
    val thumbnail: String,
    @field:Json(name = "Title")
    val title: String,
    @field:Json(name = "Visibility")
    val visibility: Boolean
)
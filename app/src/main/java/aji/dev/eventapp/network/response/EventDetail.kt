package aji.dev.eventapp.network.response

import com.squareup.moshi.Json

data class EventDetail(

    @field:Json(name = "ID")
    var id: Int? = null,
    @field:Json(name = "Code")
    var code: String? = null,
    @field:Json(name = "Background")
    var background: String? = null,
    @field:Json(name = "Text")
    var text: String? = null,
    @field:Json(name = "Thumbnail")
    var thumbnail: String? = null,
    @field:Json(name = "Visibility")
    var visibility: Boolean? = null,
    @field:Json(name = "Title")
    var title: String? = null,
    @field:Json(name = "Description")
    var description: String? = null,
    @field:Json(name = "RequireApproval")
    var requireApproval: Boolean? = null,
    @field:Json(name = "Email")
    var email: String? = null,
    @field:Json(name = "Phone")
    var phone: String? = null,
    @field:Json(name = "CreatedAt")
    var createdAt: String? = null,
    @field:Json(name = "PresentedID")
    var presentedID: Int? = null,
    @field:Json(name = "EventTicketInfo")
    var eventTicketInfo: EventTicketInfo? = EventTicketInfo(),
    @field:Json(name = "EventLocation")
    var eventLocation: EventLocation? = EventLocation(),
    @field:Json(name = "EventDate")
    var eventDate: EventListDate? = EventListDate(),
    @field:Json(name = "EventHost")
    var eventHost: List<EventHost> = emptyList(),
    @field:Json(name = "EventQuestions")
    var eventQuestions: List<EventQuestions> = emptyList(),
    @field:Json(name = "EventPresented")
    var eventPresented: EventPresented? = EventPresented(),
    @field:Json(name = "EventMember")
    var eventMember: List<String> = emptyList(),
    @field:Json(name = "EventPrice")
    var eventPrice: List<EventPrice> = emptyList(),
    @field:Json(name = "IsSubscribe")
    var isSubscribe: Boolean? = null,
    @field:Json(name = "IsSubscribeTotal")
    var isSubscribeTotal: Int? = null,
    @field:Json(name = "IsManageAccess")
    var isManageAccess: Boolean? = null

)
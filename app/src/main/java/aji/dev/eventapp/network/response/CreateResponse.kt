package aji.dev.eventapp.network.response

import com.squareup.moshi.Json

data class CreateResponse(
    @field:Json(name = "Errors")
    var errors: List<Errors> = emptyList(),
    @field:Json(name = "Message")
    var message: String = "",
    @field:Json(name = "Status")
    var status: Status = Status()
)

data class Errors(
    @field:Json(name = "Field")
    var field: String? = null,
    @field:Json(name = "Message")
    var message: String? = null

)
package aji.dev.eventapp.network.response

import com.squareup.moshi.Json

data class EventListLocation(
    @field:Json(name = "AddressFull")
    val addressFull: String,
    @field:Json(name = "Type")
    val type: String
)
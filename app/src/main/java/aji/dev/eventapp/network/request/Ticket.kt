package aji.dev.eventapp.network.request

data class Ticket(
    val currency: String,
    val description: String,
    val key: String,
    val label: String,
    val price: Int
)
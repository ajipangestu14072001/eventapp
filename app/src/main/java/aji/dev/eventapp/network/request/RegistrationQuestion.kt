package aji.dev.eventapp.network.request

data class RegistrationQuestion(
    val label: String,
    val options: List<String> = emptyList(),
    val question_type: String,
    val required: Boolean
)
package aji.dev.eventapp.network.response

import com.squareup.moshi.Json

data class EventPrice(

    @field:Json(name ="Fee")
    var fee: Int? = null,
    @field:Json(name ="Currency")
    var currency: String? = null,
    @field:Json(name ="Label")
    var label: String? = null,
    @field:Json(name ="Description")
    var description: String? = null

)
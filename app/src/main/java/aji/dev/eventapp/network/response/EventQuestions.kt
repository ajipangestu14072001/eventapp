package aji.dev.eventapp.network.response

import com.squareup.moshi.Json

data class EventQuestions(

    @field:Json(name = "Label")
    var label: String? = null,
    @field:Json(name = "Required")
    var required: Boolean? = null,
    @field:Json(name = "Options")
    var options: String? = null,
    @field:Json(name = "QuestionType")
    var questionType: String? = null

)
package aji.dev.eventapp.network.request

data class Price(
    val currency: String,
    val description: String,
    val fee: Int,
    val label: String
)
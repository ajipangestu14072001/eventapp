package aji.dev.eventapp.network.response

import com.squareup.moshi.Json

data class Status(
    @field:Json(name = "Code")
    val code: Int = 0,
    @field:Json(name = "Message")
    val message: String = ""
)
package aji.dev.eventapp.network.response

import com.squareup.moshi.Json

data class UploadResponse(
    @field:Json(name ="Results" )
    var results : Upload? = Upload(),
    @field:Json(name ="Status"  )
    var status  : Status?  = Status()
)

package aji.dev.eventapp.network.request

data class Info(
    val city: String,
    val country: String,
    val province: String
)
package aji.dev.eventapp.network.request

data class Location(
    val address: String,
    val address_full: String,
    val info: Info,
    val lat: Double,
    val lng: Double,
    val type: String
)
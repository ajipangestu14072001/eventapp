package aji.dev.eventapp.network.response

import com.squareup.moshi.Json

data class EventPresented (

    @field:Json(name ="Avatar" )
    var avatar : String? = null,
    @field:Json(name ="Name"   )
    var name   : String? = null,
    @field:Json(name ="Email"  )
    var email  : String? = null

)
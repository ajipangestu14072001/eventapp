package aji.dev.eventapp.network

import aji.dev.eventapp.network.request.CreateRequest
import aji.dev.eventapp.network.request.LoginRequest
import aji.dev.eventapp.network.request.RegisterRequest
import aji.dev.eventapp.network.response.AuthResponse
import aji.dev.eventapp.network.response.CreateResponse
import aji.dev.eventapp.network.response.DetailResponse
import aji.dev.eventapp.network.response.EventResponse
import aji.dev.eventapp.network.response.UploadResponse
import okhttp3.MultipartBody
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Multipart
import retrofit2.http.POST
import retrofit2.http.Part
import retrofit2.http.Path

interface ApiServices {
    @POST("auth/login")
    suspend fun getLogin(@Body loginRequest: LoginRequest): AuthResponse

    @POST("auth/register")
    suspend fun getRegister(@Body registerRequest: RegisterRequest): AuthResponse

    @GET("v1/event/list")
    suspend fun getListEvent(@Header("Authorization") token: String = ""): EventResponse

    @Multipart
    @POST("file/upload")
    suspend fun uploadImage(
        @Part title: MultipartBody.Part,
        @Part category: MultipartBody.Part,
        @Part file: MultipartBody.Part
    ): UploadResponse

    @POST("v1/event/create")
    suspend fun createEvent(
        @Header("Authorization") token: String = "",
        @Body createRequest: CreateRequest
    ): CreateResponse

    @GET("v1/event/show/{code}")
    suspend fun showEvent(
        @Header("Authorization") token: String = "",
        @Path("code") code: String
    ): DetailResponse

}
package aji.dev.eventapp.network.response

import com.squareup.moshi.Json

data class AuthResponse(
    @field:Json(name = "Results")
    val results: Results,
    @field:Json(name = "Status")
    val status: Status
)
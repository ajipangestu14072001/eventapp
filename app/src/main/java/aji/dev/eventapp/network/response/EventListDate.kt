package aji.dev.eventapp.network.response

import com.squareup.moshi.Json

data class EventListDate(
    @field:Json(name = "End")
    val end: String = "",
    @field:Json(name = "OneDay")
    val oneDay: Boolean = true,
    @field:Json(name = "Start")
    val start: String = "",
    @field:Json(name = "Time")
    val time: String = "",
    @field:Json(name = "TimeZone")
    val timeZone: String = ""
)
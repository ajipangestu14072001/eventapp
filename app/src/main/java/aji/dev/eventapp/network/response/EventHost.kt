package aji.dev.eventapp.network.response

import com.squareup.moshi.Json

data class EventHost(

    @field:Json(name ="Name")
    var name: String? = null

)
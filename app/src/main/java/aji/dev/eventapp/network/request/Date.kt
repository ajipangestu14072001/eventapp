package aji.dev.eventapp.network.request

data class Date(
    val end: String,
    val start: String,
    val time: String,
    val time_zone: String
)
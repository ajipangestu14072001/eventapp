package aji.dev.eventapp.repository

import aji.dev.eventapp.network.ApiServices
import aji.dev.eventapp.network.request.CreateRequest
import aji.dev.eventapp.network.request.LoginRequest
import aji.dev.eventapp.network.request.RegisterRequest
import aji.dev.eventapp.network.response.AuthResponse
import aji.dev.eventapp.network.response.CreateResponse
import aji.dev.eventapp.network.response.DetailResponse
import aji.dev.eventapp.network.response.EventResponse
import aji.dev.eventapp.network.response.UploadResponse
import okhttp3.MultipartBody
import javax.inject.Inject

class EventRepository @Inject constructor(
    private val api: ApiServices
) {
    suspend fun getLogin(loginRequest: LoginRequest): AuthResponse {
        return api.getLogin(loginRequest = loginRequest)
    }

    suspend fun getRegister(registerRequest: RegisterRequest): AuthResponse {
        return api.getRegister(registerRequest = registerRequest)
    }

    suspend fun getEvent(token: String): EventResponse {
        return api.getListEvent(token = token)
    }

    suspend fun uploadImage(
        title: MultipartBody.Part,
        category: MultipartBody.Part,
        file: MultipartBody.Part
    ): UploadResponse {
        return api.uploadImage(title = title, category = category, file = file)
    }

    suspend fun createEvent(token: String, createRequest: CreateRequest): CreateResponse {
        return api.createEvent(token = token, createRequest = createRequest)
    }
    suspend fun showEvent(token: String, code: String): DetailResponse {
        return api.showEvent(token = token, code = code)
    }
}
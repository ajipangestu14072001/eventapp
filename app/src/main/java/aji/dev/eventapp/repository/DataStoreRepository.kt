package aji.dev.eventapp.repository

import android.content.Context
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.core.stringPreferencesKey
import aji.dev.eventapp.repository.DataStoreRepository.PreferencesKey.USER_TOKEN_KEY
import androidx.datastore.preferences.preferencesDataStore
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map


val Context.dataStore: DataStore<Preferences> by preferencesDataStore(name = "event_pref")
class DataStoreRepository(context: Context) {
    private object PreferencesKey {
        val USER_TOKEN_KEY = stringPreferencesKey("token")
    }
    private val dataStore = context.dataStore


    val getToken: Flow<String?> = context.dataStore.data
        .map { preferences ->
            preferences[USER_TOKEN_KEY] ?: ""
        }

    suspend fun saveTokenAndId(token: String) {
        dataStore.edit { preferences ->
            preferences[USER_TOKEN_KEY] = token
        }
    }
}
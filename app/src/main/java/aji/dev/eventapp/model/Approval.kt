package aji.dev.eventapp.model

enum class Approval {
    Yes,
    No,
    Other
}
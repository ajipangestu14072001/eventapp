package aji.dev.eventapp.model

enum class EventType {
    Online,
    Offline,
    Other
}
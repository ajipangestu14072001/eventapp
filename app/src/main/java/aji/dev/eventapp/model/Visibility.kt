package aji.dev.eventapp.model

enum class Visibility {
    Public,
    Private,
    Other
}
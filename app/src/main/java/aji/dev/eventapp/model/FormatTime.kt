package aji.dev.eventapp.model

enum class FormatTime {
    WIB,
    WITA,
    WIT
}
package aji.dev.eventapp.model

import androidx.compose.ui.graphics.vector.ImageVector

data class EventOption(
    val name: String,
    val icon: ImageVector
)
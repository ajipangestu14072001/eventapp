package aji.dev.eventapp.navigation

import aji.dev.eventapp.model.UserData
import aji.dev.eventapp.view.CheckInScreen
import aji.dev.eventapp.view.CreateScreen
import aji.dev.eventapp.view.LoginScreen
import aji.dev.eventapp.view.MainScreen
import aji.dev.eventapp.view.MyEventScreen
import aji.dev.eventapp.view.ProfileScreen
import aji.dev.eventapp.view.RegisterScreen
import android.util.Log
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.navigation.NavHostController
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.navArgument

@Composable
fun Navigation(
    navController: NavHostController,
    destination: String
) {
    NavHost(
        navController = navController,
        startDestination = destination
    ) {
        composable(route = Screen.Main.route) {
            MainScreen(navController = navController)
        }
        composable(route = Screen.Create.route) {
            CreateScreen(navController = navController)
        }
        composable(route = Screen.Login.route) {
            LoginScreen(navController = navController)
        }
        composable(route = Screen.Register.route) {
            RegisterScreen(navController = navController)
        }
        composable(route = Screen.MyEvent.route) {
            MyEventScreen(navController = navController)
        }
        composable(route = Screen.CheckIn.route) {
            CheckInScreen(navController = navController)
        }
        composable(
            route = Screen.Profile.route,
        ) {
            val userData =
                navController.previousBackStackEntry?.savedStateHandle?.get<UserData>("userData")
            ProfileScreen(navController = navController, userData = userData)
        }
    }
}
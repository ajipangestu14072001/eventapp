package aji.dev.eventapp.navigation

sealed class Screen (val route: String){
    data object Main : Screen(route = "main")
    data object Create : Screen(route = "create")
    data object Login : Screen(route = "login")
    data object Register : Screen(route = "register")
    data object MyEvent : Screen(route = "myEvent")
    data object Profile : Screen(route = "profile")
    data object CheckIn : Screen(route = "checkIn")
}
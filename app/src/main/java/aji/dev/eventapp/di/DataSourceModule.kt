package aji.dev.eventapp.di

import aji.dev.eventapp.scanner.ScanLDS
import aji.dev.eventapp.scanner.ScanLocalDataSource
import androidx.annotation.OptIn
import androidx.camera.core.ExperimentalGetImage
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
abstract class DataSourceModule {

    @OptIn(ExperimentalGetImage::class) @Binds
    @Singleton
    abstract fun bindsScanLocalDataSource(
        scanLDS: ScanLDS
    ): ScanLocalDataSource

}
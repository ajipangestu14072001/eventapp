package aji.dev.eventapp

import aji.dev.eventapp.navigation.Navigation
import aji.dev.eventapp.navigation.Screen
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import aji.dev.eventapp.ui.theme.EventAppTheme
import aji.dev.eventapp.view.LoginScreen
import aji.dev.eventapp.view.RegisterScreen
import androidx.navigation.compose.rememberNavController
import com.mapbox.common.MapboxOptions
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        MapboxOptions.accessToken = "sk.eyJ1IjoiN3VucG9uIiwiYSI6ImNsdHpqZHQ0bDAwcGcybm85dWg3Nnh5OGoifQ._EFd7GExv8HZ8ed27CMBcQ"
        setContent {
            EventAppTheme {
                val navController = rememberNavController()
                Navigation(
                    navController = navController,
                    destination = Screen.Main.route
                )
            }
        }
    }
}
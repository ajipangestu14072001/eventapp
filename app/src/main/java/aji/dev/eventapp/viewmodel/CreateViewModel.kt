package aji.dev.eventapp.viewmodel

import aji.dev.eventapp.base.BaseErrorHandler
import aji.dev.eventapp.base.BaseViewModel
import aji.dev.eventapp.network.request.CreateRequest
import aji.dev.eventapp.repository.EventRepository
import aji.dev.eventapp.state.CreateState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import okhttp3.MultipartBody
import org.json.JSONException
import org.json.JSONObject
import retrofit2.HttpException
import javax.inject.Inject

@HiltViewModel
class CreateViewModel @Inject constructor(
    private val repository: EventRepository
) : BaseErrorHandler<CreateState>(CreateState.initialState) {

    suspend fun uploadImage(
        title: MultipartBody.Part,
        category: MultipartBody.Part,
        file: MultipartBody.Part
    ) = withContext(Dispatchers.Default) {
        setState { copy(isLoading = true) }
        val upload = try {
            repository.uploadImage(title = title, category = category, file = file)
        } catch (e: HttpException) {
            val errorResponse = e.response()?.errorBody()?.string()
            val errorMessage = parseErrorResponse(errorResponse)
            setState { copy(isLoading = false, error = errorMessage ?: "Unknown error occurred") }
            return@withContext
        }
        setState { copy(isLoading = false, data = upload) }
    }

    suspend fun createEvent(
        token: String,
        createRequest: CreateRequest
    ) = withContext(Dispatchers.Default) {
        setState { copy(isLoading = true) }
        val create = try {
            repository.createEvent(token = token, createRequest = createRequest)
        } catch (e: HttpException) {
            val errorResponse = e.response()?.errorBody()?.string()
            val errorMessage = parseErrorResponse(errorResponse)
            setState { copy(isLoading = false, error = errorMessage ?: "Unknown error occurred") }
            return@withContext
        }
        setState { copy(isLoading = false, create = create) }
    }
}


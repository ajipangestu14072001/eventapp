package aji.dev.eventapp.viewmodel

import aji.dev.eventapp.base.BaseErrorHandler
import aji.dev.eventapp.base.BaseViewModel
import aji.dev.eventapp.network.request.LoginRequest
import aji.dev.eventapp.network.request.RegisterRequest
import aji.dev.eventapp.repository.EventRepository
import aji.dev.eventapp.state.AuthState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import retrofit2.HttpException
import javax.inject.Inject

@HiltViewModel
class AuthViewModel @Inject constructor(
    private val repository: EventRepository
) : BaseErrorHandler<AuthState>(AuthState.initialState) {

    suspend fun getLogin(
        username: String, password: String
    ) = withContext(Dispatchers.Default) {
        setState { copy(isLoading = true) }
        val login = try {
            val loginRequest = LoginRequest(username, password)
            repository.getLogin(loginRequest = loginRequest)
        } catch (e: HttpException) {
            val errorResponse = e.response()?.errorBody()?.string()
            val errorMessage = parseErrorResponse(errorResponse)
            setState { copy(isLoading = false, error = errorMessage ?: "Unknown error occurred") }
            return@withContext
        }
        setState { copy(isLoading = false, data = login) }
    }

    suspend fun getRegister(
        name: String,
        email: String,
        password: String,
        phone: String
    ) = withContext(Dispatchers.Default) {
        setState { copy(isLoading = true) }
        val regis = try {
            val registerRequest = RegisterRequest(
                name = name,
                email = email,
                password = password,
                phone = phone
            )
            repository.getRegister(registerRequest = registerRequest)
        } catch (e: HttpException) {
            val errorResponse = e.response()?.errorBody()?.string()
            val errorMessage = parseErrorResponse(errorResponse)
            setState { copy(isLoading = false, error = errorMessage ?: "Unknown error occurred") }
            return@withContext
        }
        setState { copy(isLoading = false, data = regis) }
    }

}
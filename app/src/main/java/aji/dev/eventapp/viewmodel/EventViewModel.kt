package aji.dev.eventapp.viewmodel

import aji.dev.eventapp.base.BaseErrorHandler
import aji.dev.eventapp.base.BaseViewModel
import aji.dev.eventapp.network.response.Event
import aji.dev.eventapp.network.response.EventDetail
import aji.dev.eventapp.repository.EventRepository
import aji.dev.eventapp.state.EventState
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import retrofit2.HttpException
import javax.inject.Inject

@HiltViewModel
class EventViewModel @Inject constructor(
    private val repository: EventRepository
) : BaseErrorHandler<EventState>(EventState.initialState) {
    private val _apiEventItems: MutableLiveData<List<Event>> = MutableLiveData()
    private val _apiEventDetail: MutableLiveData<EventDetail> = MutableLiveData()
    val apiEventItems: LiveData<List<Event>> get() = _apiEventItems
    val apiEvenDetail: LiveData<EventDetail> get() = _apiEventDetail

    suspend fun getEvent(
        token: String
    ) = withContext(Dispatchers.Default) {
        setState { copy(isLoading = true) }
        val event = try {
            repository.getEvent(token = token)
        } catch (e: HttpException) {
            val errorResponse = e.response()?.errorBody()?.string()
            val errorMessage = parseErrorResponse(errorResponse)
            setState { copy(isLoading = false, error = errorMessage ?: "Unknown error occurred") }
            return@withContext
        }
        _apiEventItems.postValue(event.results)
        setState { copy(isLoading = false, data = event) }
    }

    suspend fun showEvent(
        token: String,
        code: String
    ) = withContext(Dispatchers.Default) {
        setState { copy(isLoading = true) }
        val event = try {
            repository.showEvent(token = token, code = code)
        } catch (e: HttpException) {
            val errorResponse = e.response()?.errorBody()?.string()
            val errorMessage = parseErrorResponse(errorResponse)
            setState { copy(isLoading = false, error = errorMessage ?: "Unknown error occurred") }
            return@withContext
        }
        _apiEventDetail.postValue(event.results)
        setState { copy(isLoading = false, detail = event) }
    }
}
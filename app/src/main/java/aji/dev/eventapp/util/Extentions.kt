package aji.dev.eventapp.util

import aji.dev.eventapp.R
import aji.dev.eventapp.model.Approval
import aji.dev.eventapp.model.Border
import aji.dev.eventapp.model.EventType
import aji.dev.eventapp.model.FormatTime
import aji.dev.eventapp.model.Visibility
import aji.dev.eventapp.network.response.Event
import aji.dev.eventapp.ui.theme.fourColor
import aji.dev.eventapp.ui.theme.primaryColor
import aji.dev.eventapp.ui.theme.thirdColor
import android.widget.Toast
import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.core.RepeatMode
import androidx.compose.animation.core.animateFloat
import androidx.compose.animation.core.infiniteRepeatable
import androidx.compose.animation.core.rememberInfiniteTransition
import androidx.compose.animation.core.tween
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.RowScope
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.aspectRatio
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.imePadding
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Clear
import androidx.compose.material.icons.filled.Search
import androidx.compose.material.icons.filled.Star
import androidx.compose.material.icons.rounded.Close
import androidx.compose.material.ripple.LocalRippleTheme
import androidx.compose.material.ripple.RippleAlpha
import androidx.compose.material.ripple.RippleTheme
import androidx.compose.material3.AlertDialog
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.DropdownMenuItem
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.ExposedDropdownMenuBox
import androidx.compose.material3.ExposedDropdownMenuDefaults
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Shapes
import androidx.compose.material3.Snackbar
import androidx.compose.material3.SnackbarHost
import androidx.compose.material3.SnackbarHostState
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.material3.TextField
import androidx.compose.material3.TextFieldDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableIntStateOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.rememberUpdatedState
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.composed
import androidx.compose.ui.draw.clip
import androidx.compose.ui.draw.drawBehind
import androidx.compose.ui.focus.FocusRequester
import androidx.compose.ui.focus.focusRequester
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Brush.Companion.linearGradient
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.Color.Companion.Transparent
import androidx.compose.ui.graphics.Color.Companion.White
import androidx.compose.ui.graphics.Shape
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalFocusManager
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.compose.ui.window.Dialog
import androidx.compose.ui.window.DialogProperties
import coil.compose.AsyncImagePainter
import coil.compose.rememberAsyncImagePainter
import com.airbnb.lottie.compose.LottieAnimation
import com.airbnb.lottie.compose.LottieCompositionSpec
import com.airbnb.lottie.compose.LottieConstants
import com.airbnb.lottie.compose.animateLottieCompositionAsState
import com.airbnb.lottie.compose.rememberLottieComposition
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

val InputBoxShape = Shapes(
    medium = RoundedCornerShape(14.dp)
)

fun Modifier.buttonModifier() = this
    .fillMaxWidth()
    .padding(top = 30.dp)
    .padding(horizontal = 20.dp)

@Composable
fun ButtonDefaults.buttonColor() = this.buttonColors(
    containerColor = primaryColor,
    contentColor = White
)

fun Modifier.textFieldModifier() = this
    .fillMaxWidth()
    .padding(horizontal = 20.dp)
    .padding(top = 20.dp)


@Composable
fun TextFieldDefaults.textFieldColor() = colors(
    focusedContainerColor = fourColor,
    unfocusedContainerColor = fourColor,
    disabledContainerColor = fourColor,
    focusedIndicatorColor = Transparent,
    unfocusedIndicatorColor = Transparent,
)

@Composable
fun DefaultSpacer() {
    Spacer(modifier = Modifier.width(6.dp))
    Spacer(
        modifier = Modifier
            .width(1.dp)
            .height(24.dp)
    )
}

object NoRippleTheme : RippleTheme {
    @Composable
    override fun defaultColor() = Color.Unspecified

    @Composable
    override fun rippleAlpha(): RippleAlpha = RippleAlpha(0.0f, 0.0f, 0.0f, 0.0f)
}

@Composable
fun Loading() {
    var showDialog by remember { mutableStateOf(true) }
    if (showDialog) {
        Dialog(
            onDismissRequest = { showDialog = false },
            DialogProperties(dismissOnBackPress = false, dismissOnClickOutside = false)
        ) {
            val composition by rememberLottieComposition(spec = LottieCompositionSpec.RawRes(R.raw.loading))
            val progress by animateLottieCompositionAsState(
                composition = composition,
                iterations = LottieConstants.IterateForever
            )
            LottieAnimation(
                composition = composition,
                progress = progress,
                modifier = Modifier
                    .size(120.dp)
                    .padding(top = 10.dp)
                    .background(Transparent)
            )
        }
    }
}

@Composable
fun ButtonCard(
    onClick: () -> Unit,
    content: @Composable () -> Unit
) {
    Card(
        modifier = Modifier
            .aspectRatio(ratio = 1f)
            .height(100.dp)
            .clickable(onClick = onClick),
        shape = RoundedCornerShape(16.dp)
    ) {
        Box(
            modifier = Modifier.fillMaxSize(),
            contentAlignment = Alignment.Center
        ) {
            content()
        }
    }
}

@Composable
fun CustomAlertDialog(
    message: String,
    modifier: Modifier = Modifier,
    openDialogCustom: MutableState<Boolean>
) {
    Card(
        shape = RoundedCornerShape(10.dp),
        modifier = Modifier.padding(10.dp, 5.dp, 10.dp, 10.dp),
    ) {
        Column(
            modifier = modifier.background(White)
        ) {
            Column(
                modifier = Modifier.padding(
                    end = 16.dp,
                    start = 16.dp,
                    top = 16.dp,
                    bottom = 8.dp
                )
            ) {
                Text(
                    text = "Something went wrong",
                    textAlign = TextAlign.Center,
                    fontSize = 14.sp,
                    modifier = Modifier
                        .padding(top = 5.dp)
                        .fillMaxWidth(),
                    maxLines = 2,
                    fontWeight = FontWeight.Bold,
                )
                Text(
                    text = message,
                    textAlign = TextAlign.Center,
                    fontSize = 14.sp,
                    overflow = TextOverflow.Ellipsis,
                    modifier = Modifier
                        .padding(top = 10.dp, start = 16.dp, end = 16.dp)
                        .fillMaxWidth(),
                )
            }
            Row(
                modifier = Modifier
                    .fillMaxWidth(),
                horizontalArrangement = Arrangement.Center
            ) {
                TextButton(
                    onClick = {
                        openDialogCustom.value = false
                    },
                    modifier = Modifier
                        .padding(bottom = 5.dp, top = 5.dp)
                ) {
                    Text("Confirm", fontWeight = FontWeight.Bold, color = primaryColor)
                }
            }
        }
    }
}


@Composable
fun Chip(
    deliveryOptions: String,
    selected: Boolean,
    onClick: () -> Unit,
    icon: ImageVector
) {
    CompositionLocalProvider(value = LocalRippleTheme provides NoRippleTheme) {
        Card(
            modifier = Modifier
                .padding(5.dp)
                .border(
                    width = 1.dp,
                    color = if (selected) Transparent else primaryColor,
                    shape = RoundedCornerShape(50.dp)
                )
                .clickable(onClick = onClick),
            shape = RoundedCornerShape(50.dp),
            colors = CardDefaults.cardColors(containerColor = if (selected) primaryColor else White),
            elevation = CardDefaults.cardElevation(defaultElevation = 2.dp)
        ) {
            Row(
                verticalAlignment = Alignment.CenterVertically,
                horizontalArrangement = Arrangement.Center
            ) {
                Icon(
                    imageVector = icon,
                    contentDescription = null,
                    tint = if (selected) White else primaryColor,
                    modifier = Modifier
                        .padding(start = 12.dp, end = 6.dp)
                        .size(size = 20.dp)
                )
                Text(
                    modifier = Modifier.padding(end = 12.dp, top = 6.dp, bottom = 6.dp),
                    text = deliveryOptions,
                    fontSize = 10.sp,
                    fontWeight = FontWeight.Bold,
                    color = if (selected) White else primaryColor,
                    textAlign = TextAlign.Center
                )
            }
        }
    }
}

@Composable
fun ImageSlider(images: List<Any>) {
    var currentImageIndex by remember { mutableIntStateOf(0) }
    var isAnimating by remember { mutableStateOf(false) }
    val coroutineScope = rememberCoroutineScope()
    val stableCurrentImageIndex = rememberUpdatedState(currentImageIndex)
    LazyRow(
        modifier = Modifier.fillMaxWidth(),
        horizontalArrangement = Arrangement.spacedBy(16.dp),
        contentPadding = PaddingValues(
            start = 16.dp,
            end = 16.dp,
        ),
    ) {
        itemsIndexed(images) { index, image ->
            Card(
                modifier = Modifier
                    .width(300.dp)
                    .height(150.dp)
                    .clickable {
                        if (index != stableCurrentImageIndex.value && !isAnimating) {
                            isAnimating = true
                            coroutineScope.launch {
                                val delayMillis = 500L
                                delay(timeMillis = delayMillis / 2)
                                currentImageIndex = index
                                delay(delayMillis)
                                isAnimating = false
                            }
                        }
                    },
            ) {
                ShimmerImage(
                    imageUrl = image as String,
                    placeholderResId = R.drawable.image_not_available,
                    modifier = Modifier
                        .width(300.dp)
                        .height(150.dp),
                    onClick = {

                    }
                )
            }
        }
    }
}

fun Modifier.shimmerBackground(
    targetValue: Float = 1000f,
    showShimmer: Boolean,
    shape: Shape = RoundedCornerShape(8.dp)
): Modifier = composed {

    val transition = rememberInfiniteTransition(label = "")
    lateinit var brush: Brush
    val shimmerColors = listOf(
        Color.LightGray.copy(alpha = 0.6f),
        Color.LightGray.copy(alpha = 0.2f),
        Color.LightGray.copy(alpha = 0.6f),
    )
    val translateAnimation by transition.animateFloat(
        initialValue = 0f,
        targetValue = targetValue,
        animationSpec = infiniteRepeatable(
            animation = tween(durationMillis = 800), repeatMode = RepeatMode.Reverse
        ), label = ""
    )
    brush = if (showShimmer) {
        linearGradient(
            colors = shimmerColors,
            start = Offset.Zero,
            end = Offset(x = translateAnimation, y = translateAnimation)
        )
    } else {
        linearGradient(
            colors = listOf(Transparent, Transparent),
            start = Offset.Zero,
            end = Offset.Zero
        )
    }
    return@composed this.then(background(brush, shape))
}

@Composable
fun EventItemList(eventItems: List<Event>, onItemClicked: (String) -> Unit) {
    LazyRow(
        modifier = Modifier.padding(top = 10.dp),
        horizontalArrangement = Arrangement.spacedBy(16.dp),
        contentPadding = PaddingValues(
            start = 16.dp,
            end = 16.dp,
        ),
    ) {
        items(eventItems) { event ->
            EventItemCard(event = event){
                onItemClicked(event.code)
            }
        }
    }
}

@Composable
fun EventItemCard(event: Event, onItemClick: () -> Unit) {
    Column(
        horizontalAlignment = Alignment.Start,
    ) {
        ShimmerImage(
            imageUrl = event.thumbnail,
            placeholderResId = R.drawable.image_not_available,
            border = Border(1.dp, Color.LightGray, RoundedCornerShape(8.dp)),
            modifier = Modifier
                .size(150.dp)
                .clip(RoundedCornerShape(8.dp)),
            onClick = onItemClick
        )

        Spacer(modifier = Modifier.height(6.dp))
        Text(
            modifier = Modifier.width(150.dp),
            text = event.title,
            fontSize = 16.sp,
            fontWeight = FontWeight.Bold,
            maxLines = 1,
            overflow = TextOverflow.Ellipsis,
        )

        Row(
            horizontalArrangement = Arrangement.SpaceBetween,
            modifier = Modifier
                .fillMaxWidth()
        ) {
            Text(text = " ${event.eventListLocation.type} | ", fontSize = 11.sp)
            Row(verticalAlignment = Alignment.CenterVertically) {
                Icon(
                    Icons.Default.Star,
                    contentDescription = null,
                    modifier = Modifier.size(10.dp),
                    tint = primaryColor
                )
                Spacer(modifier = Modifier.width(4.dp))
                Text(text = event.eventListDate.time, fontSize = 11.sp)
            }
        }
    }
}

@Composable
fun ListEvent(eventItem: Event, onItemClicked: (String) -> Unit) {
    Column(
        horizontalAlignment = Alignment.CenterHorizontally,
        modifier = Modifier
            .padding(vertical = 8.dp)
    ) {
        Row(
            modifier = Modifier
                .fillMaxWidth()
                .padding(horizontal = 16.dp),
            verticalAlignment = Alignment.Top
        ) {
            ShimmerImage(
                imageUrl = eventItem.thumbnail,
                placeholderResId = R.drawable.image_not_available,
                border = Border(
                    width = 1.dp,
                    color = Color.LightGray,
                    shape = RoundedCornerShape(8.dp)
                ),
                modifier = Modifier
                    .size(80.dp)
                    .clip(RoundedCornerShape(8.dp)),
                onClick = {
                    onItemClicked(eventItem.code)
                }
            )

            Spacer(modifier = Modifier.width(16.dp))

            Column(
                modifier = Modifier.clickable {
                    onItemClicked(eventItem.code)
                }
            ){
                Text(
                    modifier = Modifier.fillMaxWidth(),
                    maxLines = 1,
                    overflow = TextOverflow.Ellipsis,
                    text = eventItem.title,
                    fontWeight = FontWeight.Bold,
                    fontSize = 16.sp
                )
                Spacer(modifier = Modifier.height(4.dp))
                Row(
                    modifier = Modifier.fillMaxWidth()
                ) {
                    Text(
                        text = "${eventItem.eventListPresented.name} |",
                        fontSize = 11.sp
                    )
                    Text(
                        text = " ${eventItem.eventListLocation.type} | ",
                        fontSize = 11.sp
                    )
                    Row(verticalAlignment = Alignment.CenterVertically) {
                        Icon(
                            Icons.Default.Star,
                            contentDescription = null,
                            modifier = Modifier.size(10.dp),
                            tint = primaryColor
                        )
                        Spacer(modifier = Modifier.width(4.dp))
                        Text(text = eventItem.eventListDate.time, fontSize = 11.sp)
                    }
                }
                Spacer(modifier = Modifier.height(4.dp))
                Text(
                    overflow = TextOverflow.Ellipsis,
                    maxLines = 2,
                    lineHeight = 13.sp,
                    text = eventItem.eventListLocation.addressFull,
                    fontSize = 11.sp
                )

            }
        }
    }
}

@Composable
fun BadgeBoxIcon(
    badge: @Composable () -> Unit,
    notificationRadius: Dp = 8.dp,
    notification: Boolean,
    notificationCount: Int = 0
) {

    Box {
        Box(modifier = Modifier.padding(notificationRadius)) {
            badge()
        }

        Box(
            modifier = if (notification) Modifier
                .padding(notificationRadius / 2)
                .align(Alignment.TopEnd)
                .size(notificationRadius * 2)
                .drawBehind {
                    drawCircle(Color.Red)
                } else Modifier) {
            if (notificationCount > 0) {
                Text(
                    text = notificationCount.toString(),
                    fontSize = 10.sp,
                    color = White,
                    textAlign = TextAlign.Center,
                    modifier = Modifier
                        .align(Alignment.Center)
                        .padding(bottom = 1.dp)
                )
            }
        }
    }
}

@Composable
fun ShimmerImage(
    imageUrl: String? = null,
    placeholderResId: Int,
    border: Border? = null,
    modifier: Modifier,
    onClick: () -> Unit = {}
) {
    val painter = rememberAsyncImagePainter(
        model = imageUrl,
        error = painterResource(id = placeholderResId)
    )
    val showShimmer = remember { mutableStateOf(false) }
    showShimmer.value = painter.state is AsyncImagePainter.State.Loading

    val imageModifier = if (showShimmer.value || painter.state is AsyncImagePainter.State.Error) {
        modifier
            .shimmerBackground(showShimmer = showShimmer.value)
            .fillMaxWidth()
            .border(
                width = border?.width ?: 0.dp,
                color = border?.color ?: Transparent,
                shape = border?.shape ?: RoundedCornerShape(0.dp)
            )
            .clip(border?.shape ?: RoundedCornerShape(0.dp))
    } else {
        modifier
            .shimmerBackground(showShimmer = showShimmer.value)
            .fillMaxWidth()
            .clip(shape = RoundedCornerShape(0.dp))
    }


    Image(
        painter = painter,
        contentDescription = null,
        contentScale = ContentScale.Crop,
        modifier = imageModifier.clickable(onClick = onClick)
    )
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun SearchBar(
    autoFocus: Boolean,
    onSearch: () -> Unit
) {
    Box(
        modifier = Modifier
            .padding(all = 10.dp)
            .clip(RoundedCornerShape(percent = 20))
            .background(Color.LightGray)
            .height(54.dp)
    ) {
        var searchInput: String by remember { mutableStateOf("") }
        val focusRequester = remember { FocusRequester() }
        val focusManager = LocalFocusManager.current

        LaunchedEffect(key1 = searchInput) {
//            if (viewModel.searchParam.value.trim().isNotEmpty() &&
//                viewModel.searchParam.value.trim().length != viewModel.previousSearch.value.length
//            ) {
//                delay(750)
//                onSearch()
//                viewModel.previousSearch.value = searchInput.trim()
//            }
        }

        TextField(
            value = searchInput,
            onValueChange = { newValue ->
                searchInput = if (newValue.trim().isNotEmpty()) newValue else ""
//                viewModel.searchParam.value = searchInput
            },
            modifier = Modifier
                .fillMaxSize()
                .focusRequester(focusRequester = focusRequester),
            singleLine = true,
            placeholder = {
                Text(
                    text = "Search...",
                    color = Color.Gray
                )
            },
            colors = ExposedDropdownMenuDefaults.textFieldColors(
                disabledTextColor = Color.LightGray,
                focusedIndicatorColor = Transparent,
                unfocusedIndicatorColor = Transparent
            ), keyboardOptions = KeyboardOptions(
                autoCorrect = true,
                keyboardType = KeyboardType.Text,
                imeAction = ImeAction.Search
            ),
            keyboardActions = KeyboardActions(
                onSearch = {

                }
            ),
            trailingIcon = {
                LaunchedEffect(Unit) {
                    if (autoFocus) {
                        focusRequester.requestFocus()
                    }
                }
                Row {
                    AnimatedVisibility(visible = searchInput.trim().isNotEmpty()) {
                        IconButton(onClick = {
                            focusManager.clearFocus()
                            searchInput = ""
//                            viewModel.searchParam.value = "Food"
                        }) {
                            Icon(
                                imageVector = Icons.Default.Clear,
                                contentDescription = null
                            )
                        }
                    }
                }
            },
            leadingIcon = {
                Icon(
                    imageVector = Icons.Default.Search,
                    contentDescription = null
                )
            }
        )
    }
}

@Composable
fun KeyboardAware(
    content: @Composable () -> Unit
) {
    Box(modifier = Modifier.imePadding()) {
        content()
    }
}

fun getApprovalList(): List<Approval> {
    val approvalList = mutableListOf<Approval>()
    approvalList.add(Approval.Yes)
    approvalList.add(Approval.No)
    approvalList.add(Approval.Other)

    return approvalList
}

fun getVisibility(): List<Visibility> {
    val visibilityList = mutableListOf<Visibility>()
    visibilityList.add(Visibility.Public)
    visibilityList.add(Visibility.Private)
    visibilityList.add(Visibility.Other)

    return visibilityList
}

fun getFormatTime(): List<FormatTime> {
    val formatTimeList = mutableListOf<FormatTime>()
    formatTimeList.add(FormatTime.WIB)
    formatTimeList.add(FormatTime.WIT)
    formatTimeList.add(FormatTime.WITA)

    return formatTimeList
}

fun getEventType(): List<EventType> {
    val eventTypeList = mutableListOf<EventType>()
    eventTypeList.add(EventType.Online)
    eventTypeList.add(EventType.Offline)
    eventTypeList.add(EventType.Other)

    return eventTypeList
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun RecurrenceDropdownMenu(recurrence: (String) -> Unit) {
    Column(
        verticalArrangement = Arrangement.spacedBy(8.dp)
    ) {

        val options = getApprovalList().map { it.name }
        var expanded by remember { mutableStateOf(false) }
        var selectedOptionText by remember { mutableStateOf(options[0]) }

        Text(
            text = "Need Approval",
            style = MaterialTheme.typography.bodyLarge
        )

        ExposedDropdownMenuBox(
            expanded = expanded,
            onExpandedChange = { expanded = !expanded },
        ) {
            TextField(
                modifier = Modifier.menuAnchor(),
                readOnly = true,
                value = selectedOptionText,
                onValueChange = {},
                trailingIcon = { ExposedDropdownMenuDefaults.TrailingIcon(expanded = expanded) },
                colors = ExposedDropdownMenuDefaults.textFieldColors(),
            )
            ExposedDropdownMenu(
                expanded = expanded,
                onDismissRequest = { expanded = false },
            ) {
                options.forEach { selectionOption ->
                    DropdownMenuItem(
                        text = { Text(selectionOption) },
                        onClick = {
                            selectedOptionText = selectionOption
                            recurrence(selectionOption)
                            expanded = false
                        }
                    )
                }
            }
        }
    }
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun VisibilityDropdownMenu(recurrence: (String) -> Unit) {
    Column(
        modifier = Modifier.width(128.dp),
        verticalArrangement = Arrangement.spacedBy(8.dp)
    ) {

        val options = getVisibility().map { it.name }
        var expanded by remember { mutableStateOf(false) }
        var selectedOptionText by remember { mutableStateOf(options[0]) }

        Text(
            text = "Visibility",
            style = MaterialTheme.typography.bodyLarge
        )

        ExposedDropdownMenuBox(
            expanded = expanded,
            onExpandedChange = { expanded = !expanded },
        ) {
            TextField(
                modifier = Modifier.menuAnchor(),
                readOnly = true,
                value = selectedOptionText,
                onValueChange = {},
                trailingIcon = { ExposedDropdownMenuDefaults.TrailingIcon(expanded = expanded) },
                colors = ExposedDropdownMenuDefaults.textFieldColors(),
            )
            ExposedDropdownMenu(
                expanded = expanded,
                onDismissRequest = { expanded = false },
            ) {
                options.forEach { selectionOption ->
                    DropdownMenuItem(
                        text = { Text(selectionOption) },
                        onClick = {
                            selectedOptionText = selectionOption
                            recurrence(selectionOption)
                            expanded = false
                        }
                    )
                }
            }
        }
    }
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun FormatTimeDropdownMenu(recurrence: (String) -> Unit, modifier: Modifier) {
    Column(
        modifier = modifier,
        verticalArrangement = Arrangement.spacedBy(8.dp)
    ) {

        val options = getFormatTime().map { it.name }
        var expanded by remember { mutableStateOf(false) }
        var selectedOptionText by remember { mutableStateOf(options[0]) }

        Text(
            text = "Time Format",
            style = MaterialTheme.typography.bodyLarge
        )

        ExposedDropdownMenuBox(
            expanded = expanded,
            onExpandedChange = { expanded = !expanded },
        ) {
            TextField(
                modifier = Modifier.menuAnchor(),
                readOnly = true,
                value = selectedOptionText,
                onValueChange = {},
                trailingIcon = { ExposedDropdownMenuDefaults.TrailingIcon(expanded = expanded) },
                colors = ExposedDropdownMenuDefaults.textFieldColors(),
            )
            ExposedDropdownMenu(
                expanded = expanded,
                onDismissRequest = { expanded = false },
            ) {
                options.forEach { selectionOption ->
                    DropdownMenuItem(
                        text = { Text(selectionOption) },
                        onClick = {
                            selectedOptionText = selectionOption
                            recurrence(selectionOption)
                            expanded = false
                        }
                    )
                }
            }
        }
    }
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun EventTypeDropdownMenu(recurrence: (String) -> Unit, modifier: Modifier) {
    Column(
        modifier = modifier,
        verticalArrangement = Arrangement.spacedBy(8.dp)
    ) {

        val options = getEventType().map { it.name }
        var expanded by remember { mutableStateOf(false) }
        var selectedOptionText by remember { mutableStateOf(options[0]) }

        Text(
            text = "Event Type",
            style = MaterialTheme.typography.bodyLarge
        )

        ExposedDropdownMenuBox(
            expanded = expanded,
            onExpandedChange = { expanded = !expanded },
        ) {
            TextField(
                modifier = Modifier.menuAnchor(),
                readOnly = true,
                value = selectedOptionText,
                onValueChange = {},
                trailingIcon = { ExposedDropdownMenuDefaults.TrailingIcon(expanded = expanded) },
                colors = ExposedDropdownMenuDefaults.textFieldColors(),
            )
            ExposedDropdownMenu(
                expanded = expanded,
                onDismissRequest = { expanded = false },
            ) {
                options.forEach { selectionOption ->
                    DropdownMenuItem(
                        text = { Text(selectionOption) },
                        onClick = {
                            selectedOptionText = selectionOption
                            recurrence(selectionOption)
                            expanded = false
                        }
                    )
                }
            }
        }
    }
}
package aji.dev.eventapp.util

import aji.dev.eventapp.model.EventOption
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.rounded.DateRange
import androidx.compose.material.icons.rounded.LocationOn
import androidx.compose.material.icons.rounded.Star

object Constant {
    const val BASE_URL = "https://www.api.saget.id/"
    val eventOptions = listOf(
        EventOption(name = "All Event", icon = Icons.Rounded.Star),
        EventOption(name = "New Event", icon = Icons.Rounded.LocationOn),
        EventOption(name = "Near Event", icon = Icons.Rounded.DateRange),
    )
    val images = listOf(
        "https://lelogama.go-jek.com/post_featured_image/Consumer_Trends_Blog_Banner_1.jpg",
        "https://lelogama.go-jek.com/post_featured_image/LDUI_Blog_Banner_1.jpg",
        "https://gadgetsquad.id/wp-content/uploads/2022/07/airasia-food-1.jpg",
        "https://mmc.tirto.id/image/otf/1024x535/2021/05/20/713b6f5a13396c07924b26ca3986482e_ratio-16x9.jpg",
        "https://i0.wp.com/pointsgeek.id/wp-content/uploads/2021/07/Screen-Shot-2021-07-26-at-10.17.33.png?fit=759%2C408&ssl=1",
    )
}
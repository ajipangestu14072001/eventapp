package aji.dev.eventapp.view

import aji.dev.eventapp.component.DateTextField
import aji.dev.eventapp.model.Approval
import aji.dev.eventapp.model.EventType
import aji.dev.eventapp.model.FormatTime
import aji.dev.eventapp.model.Visibility
import aji.dev.eventapp.navigation.Screen
import aji.dev.eventapp.network.request.CreateRequest
import aji.dev.eventapp.network.request.Date
import aji.dev.eventapp.network.request.Info
import aji.dev.eventapp.network.request.Location
import aji.dev.eventapp.network.request.Price
import aji.dev.eventapp.network.request.RegistrationQuestion
import aji.dev.eventapp.network.request.Ticket
import aji.dev.eventapp.repository.DataStoreRepository
import aji.dev.eventapp.util.ButtonCard
import aji.dev.eventapp.util.CustomAlertDialog
import aji.dev.eventapp.util.EventTypeDropdownMenu
import aji.dev.eventapp.util.FormatTimeDropdownMenu
import aji.dev.eventapp.util.InputBoxShape
import aji.dev.eventapp.util.Loading
import aji.dev.eventapp.util.RecurrenceDropdownMenu
import aji.dev.eventapp.util.VisibilityDropdownMenu
import aji.dev.eventapp.util.buttonColor
import aji.dev.eventapp.util.buttonModifier
import aji.dev.eventapp.viewmodel.CreateViewModel
import android.net.Uri
import android.widget.Toast
import androidx.activity.compose.rememberLauncherForActivityResult
import androidx.activity.result.PickVisualMediaRequest
import androidx.activity.result.contract.ActivityResultContracts
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.automirrored.rounded.KeyboardArrowLeft
import androidx.compose.material.icons.rounded.Add
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.material3.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableLongStateOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.TextRange
import androidx.compose.ui.text.input.TextFieldValue
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.compose.ui.window.Dialog
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavHostController
import coil.compose.AsyncImage
import kotlinx.coroutines.launch
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody.Companion.asRequestBody
import java.io.File

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun CreateScreen(
    navController: NavHostController,
    createViewModel: CreateViewModel = hiltViewModel(),
) {
    var selectedImageUri by remember {
        mutableStateOf<Uri?>(value = null)
    }
    var title by rememberSaveable(stateSaver = TextFieldValue.Saver) {
        mutableStateOf(TextFieldValue("", TextRange(0, 7)))
    }
    var address by rememberSaveable(stateSaver = TextFieldValue.Saver) {
        mutableStateOf(TextFieldValue("", TextRange(0, 7)))
    }
    var city by rememberSaveable(stateSaver = TextFieldValue.Saver) {
        mutableStateOf(TextFieldValue("", TextRange(0, 7)))
    }
    var province by rememberSaveable(stateSaver = TextFieldValue.Saver) {
        mutableStateOf(TextFieldValue("", TextRange(0, 7)))
    }
    var email by rememberSaveable(stateSaver = TextFieldValue.Saver) {
        mutableStateOf(TextFieldValue("", TextRange(0, 7)))
    }
    var phone by rememberSaveable(stateSaver = TextFieldValue.Saver) {
        mutableStateOf(TextFieldValue("", TextRange(0, 7)))
    }
    var fullAddress by rememberSaveable(stateSaver = TextFieldValue.Saver) {
        mutableStateOf(TextFieldValue("", TextRange(0, 7)))
    }
    var desc by rememberSaveable(stateSaver = TextFieldValue.Saver) {
        mutableStateOf(TextFieldValue("", TextRange(0, 7)))
    }
    val shouldShowDialog = remember { mutableStateOf(value = false) }

    var visibility by rememberSaveable { mutableStateOf(value = Visibility.Public.name) }
    var recurrence by rememberSaveable { mutableStateOf(value = Approval.Yes.name) }
    var formatTime by rememberSaveable { mutableStateOf(value = FormatTime.WIB.name) }
    var eventType by rememberSaveable { mutableStateOf(value = EventType.Online.name) }
    var showDialog by remember { mutableStateOf(value = false) }
    var starDate by rememberSaveable { mutableLongStateOf(java.util.Date().time) }
    var endDate by rememberSaveable { mutableLongStateOf(java.util.Date().time) }

    val context = LocalContext.current
    val dataStore = DataStoreRepository(context)
    val token = dataStore.getToken.collectAsState(initial = "")
    val scope = rememberCoroutineScope()
    val singlePhotoPickerLauncher = rememberLauncherForActivityResult(
        contract = ActivityResultContracts.PickVisualMedia(),
        onResult = { uri ->
            uri?.let { selectedUri ->
                if (selectedUri.scheme == "content") {
                    showDialog = true
                    val inputStream = context.contentResolver.openInputStream(selectedUri)
                    if (inputStream != null) {
                        val file = File(context.cacheDir, "picked_image")
                        file.outputStream().use { outputStream ->
                            inputStream.copyTo(outputStream)
                        }
                        val requestBody = file.asRequestBody("image/*".toMediaType())
                        val filePart =
                            MultipartBody.Part.createFormData(name = "file", file.name, requestBody)
                        val titlePart = MultipartBody.Part.createFormData(name = "title", value = "tkinter")
                        val categoryPart = MultipartBody.Part.createFormData(name = "category", value = "aji@gmail.com")

                        scope.launch {
                            createViewModel.uploadImage(
                                title = titlePart,
                                category = categoryPart,
                                file = filePart
                            )
                        }
                    }
                }
            }
        }
    )

    val stateUpload by createViewModel.state.collectAsState()
    LaunchedEffect(key1 = stateUpload.data) {
        stateUpload.data?.let {
            selectedImageUri = Uri.parse(it.results?.url)
        }
    }
    if (showDialog) Loading()
    if (shouldShowDialog.value) {
        Dialog(onDismissRequest = { shouldShowDialog.value = false }) {
            CustomAlertDialog(
                openDialogCustom = shouldShowDialog,
                message = stateUpload.error ?: "Unknown error occurred"
            )
        }
    }

    Scaffold(
        topBar = {
            TopAppBar(
                title = {
                    Text(
                        text = "Create Event",
                        fontSize = 16.sp
                    )
                },
                navigationIcon = {
                    IconButton(
                        onClick = {
                            navController.navigate(Screen.Main.route)
                        }
                    ) {
                        Icon(
                            Icons.AutoMirrored.Rounded.KeyboardArrowLeft,
                            contentDescription = null
                        )
                    }
                }
            )
        },
        content = { padding ->
            Column(
                modifier = Modifier
                    .fillMaxHeight()
                    .padding(paddingValues = padding)
                    .padding(horizontal = 16.dp, vertical = 8.dp),
                verticalArrangement = Arrangement.SpaceBetween,

                ) {

                Column(
                    modifier = Modifier
                        .verticalScroll(rememberScrollState())
                        .weight(1f, false)

                ) {
                    ButtonCard(onClick = {
                        singlePhotoPickerLauncher.launch(
                            PickVisualMediaRequest(ActivityResultContracts.PickVisualMedia.ImageOnly)
                        )
                    }) {
                        selectedImageUri?.let { uri ->
                            showDialog = false
                            AsyncImage(
                                model = uri,
                                contentDescription = null,
                                modifier = Modifier.fillMaxSize(),
                                contentScale = ContentScale.Crop
                            )
                        } ?: Icon(
                            imageVector = Icons.Rounded.Add,
                            contentDescription = "Upload icon",
                            modifier = Modifier.size(24.dp)
                        )
                    }

                    Text(
                        modifier = Modifier
                            .padding(top = 16.dp),
                        text = "Title Event",
                        style = MaterialTheme.typography.bodyLarge
                    )

                    TextField(
                        modifier = Modifier
                            .fillMaxWidth()
                            .padding(top = 8.dp),
                        value = title,
                        onValueChange = { title = it },
                        placeholder = { Text(text = "e.g. Google Event") }
                    )
                    Text(
                        modifier = Modifier
                            .padding(top = 16.dp),
                        text = "Event Descriptions",
                        style = MaterialTheme.typography.bodyLarge
                    )
                    TextField(
                        modifier = Modifier
                            .fillMaxWidth()
                            .padding(top = 8.dp),
                        value = desc,
                        onValueChange = { desc = it },
                        placeholder = { Text(text = "e.g. New technology on your hand") }
                    )
                    Row(
                        modifier = Modifier
                            .fillMaxWidth()
                            .padding(top = 16.dp),
                        horizontalArrangement = Arrangement.spacedBy(16.dp)
                    ) {
                        VisibilityDropdownMenu { visibility = it }
                        RecurrenceDropdownMenu { recurrence = it }
                    }

                    DateTextField(label = "Start Date", date = {
                        starDate = it
                    })

                    DateTextField(label = "End Date", date = {
                        endDate = it
                    })

                    Row(
                        modifier = Modifier
                            .fillMaxWidth()
                            .padding(top = 16.dp),
                        horizontalArrangement = Arrangement.spacedBy(16.dp)
                    ) {
                        FormatTimeDropdownMenu(
                            recurrence = { formatTime = it },
                            modifier = Modifier.weight(1f)
                        )

                        EventTypeDropdownMenu(
                            recurrence = { eventType = it },
                            modifier = Modifier.weight(1f)
                        )
                    }
                    Text(
                        modifier = Modifier
                            .padding(top = 16.dp),
                        text = "Event Address",
                        style = MaterialTheme.typography.bodyLarge
                    )

                    TextField(
                        modifier = Modifier
                            .fillMaxWidth()
                            .padding(top = 8.dp),
                        value = address,
                        onValueChange = { address = it },
                        placeholder = { Text(text = "e.g. Haji Wahab street No. 22") }
                    )

                    Text(
                        modifier = Modifier
                            .padding(top = 16.dp),
                        text = "Event Full Address",
                        style = MaterialTheme.typography.bodyLarge
                    )

                    TextField(
                        modifier = Modifier
                            .fillMaxWidth()
                            .padding(top = 8.dp),
                        value = fullAddress,
                        onValueChange = { fullAddress = it },
                        placeholder = { Text(text = "e.g. Haji Wahab street No. 22") }
                    )

                    Text(
                        modifier = Modifier
                            .padding(top = 16.dp),
                        text = "Province",
                        style = MaterialTheme.typography.bodyLarge
                    )

                    TextField(
                        modifier = Modifier
                            .fillMaxWidth()
                            .padding(top = 8.dp),
                        value = province,
                        onValueChange = { province = it },
                        placeholder = { Text(text = "e.g. East Java") }
                    )

                    Text(
                        modifier = Modifier
                            .padding(top = 16.dp),
                        text = "City",
                        style = MaterialTheme.typography.bodyLarge
                    )

                    TextField(
                        modifier = Modifier
                            .fillMaxWidth()
                            .padding(top = 8.dp),
                        value = city,
                        onValueChange = { city = it },
                        placeholder = { Text(text = "e.g. Trenggalek") }
                    )

                    Text(
                        modifier = Modifier
                            .padding(top = 16.dp),
                        text = "Email",
                        style = MaterialTheme.typography.bodyLarge
                    )

                    TextField(
                        modifier = Modifier
                            .fillMaxWidth()
                            .padding(top = 8.dp),
                        value = email,
                        onValueChange = { email = it },
                        placeholder = { Text(text = "e.g. event@event.com") }
                    )

                    Text(
                        modifier = Modifier
                            .padding(top = 16.dp),
                        text = "Phone Number",
                        style = MaterialTheme.typography.bodyLarge
                    )

                    TextField(
                        modifier = Modifier
                            .fillMaxWidth()
                            .padding(top = 8.dp),
                        value = phone,
                        onValueChange = { phone = it },
                        placeholder = { Text(text = "e.g. 081345602416") }
                    )

                }
                Button(
                    onClick = {
                        showDialog = true
                        scope.launch {
                            val info = Info(
                                country = "Indonesia",
                                province = province.text,
                                city = city.text
                            )
                            val location = Location(
                                address = address.text,
                                address_full = fullAddress.text,
                                type = visibility,
                                lat = -3.745,
                                lng = -38.745,
                                info = info
                            )
                            val date = Date(
                                start = "2003-12-03T23:00:00+07:00",
//                                start = "7:00",
                                end = "2003-12-03T00:00:00+07:00",
                                time = "21:00-23:00",
                                time_zone = formatTime
                            )
                            val ticket = Ticket(
                                key = "999346",
                                price = 1000000,
                                currency = "IDR",
                                label = "TupperWare",
                                description = "Ini deskripsi si Fulan untuk event makan besar"
                            )
                            val listTicket = listOf(
                                ticket
                            )
                            val question = RegistrationQuestion(
                                label = "Apakah kamu alergi makanan laut?",
                                required = true,
                                question_type = "text"
                            )
                            val listQuestion = listOf(
                                question
                            )
                            val price = Price(
                                label = "Paket Combo 1",
                                currency = "IDR",
                                description = "Paket Hemat",
                                fee = 500000
                            )
                            val listPrice = listOf(
                                price
                            )
                            val createRequest = CreateRequest(
                                description = desc.text,
                                title = title.text,
                                visibility = true,
                                thumbnail = stateUpload.data?.results?.url.toString(),
                                require_approval = true,
                                location = location,
                                date = date,
                                ticket = listTicket,
                                registration_questions = listQuestion,
                                email = "aji@gmail.com",
                                phone = "6281345602416",
                                price = listPrice
                            )
                            createViewModel.createEvent(token = token.value.toString(), createRequest = createRequest)
                            if (stateUpload.create?.status?.code == 200) {
                                navController.navigate(route = Screen.Main.route)
                                showDialog = false
                            }else{
                                shouldShowDialog.value = true
                                showDialog = false
                            }
                        }
                    },
                    modifier = Modifier
                        .buttonModifier()
                        .padding(bottom = 10.dp),
                    colors = ButtonDefaults.buttonColor(),
                    shape = InputBoxShape.medium,
                    contentPadding = PaddingValues(vertical = 14.dp)
                ) {
                    Text("Create Event")
                }
            }
        }
    )
}


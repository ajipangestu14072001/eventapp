package aji.dev.eventapp.view

import aji.dev.eventapp.R
import aji.dev.eventapp.model.Border
import aji.dev.eventapp.navigation.Screen
import aji.dev.eventapp.network.response.EventDetail
import aji.dev.eventapp.network.response.EventPrice
import aji.dev.eventapp.ui.theme.fourColor
import aji.dev.eventapp.ui.theme.primaryColor
import aji.dev.eventapp.util.BadgeBoxIcon
import aji.dev.eventapp.util.NoRippleTheme
import aji.dev.eventapp.util.ShimmerImage
import aji.dev.eventapp.util.buttonColor
import android.content.Intent
import android.net.Uri
import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.aspectRatio
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.grid.GridCells
import androidx.compose.foundation.lazy.grid.LazyVerticalGrid
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.automirrored.filled.ArrowForward
import androidx.compose.material.icons.rounded.DateRange
import androidx.compose.material.icons.rounded.LocationOn
import androidx.compose.material.ripple.LocalRippleTheme
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableIntStateOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.core.content.ContextCompat.startActivity
import androidx.navigation.NavHostController
import coil.compose.AsyncImage
import com.mapbox.geojson.Point
import com.mapbox.maps.MapboxExperimental
import com.mapbox.maps.extension.compose.MapboxMap
import com.mapbox.maps.extension.compose.animation.viewport.MapViewportState
import com.mapbox.maps.extension.compose.annotation.ViewAnnotation
import com.mapbox.maps.viewannotation.geometry
import com.mapbox.maps.viewannotation.viewAnnotationOptions

@OptIn(MapboxExperimental::class)
@Composable
fun DetailScreen(
    navController: NavHostController,
    eventItem: EventDetail
) {
    val listState = rememberScrollState()
    val context = LocalContext.current
    val selectedTicketIndex = remember { mutableIntStateOf(value = -1) }
    val selfLocationPoint: Point by remember {
        mutableStateOf(
            Point.fromLngLat(
                107.613144,
                -6.905977
            )
        )
    }
    Column(
        modifier = Modifier
            .fillMaxWidth()
            .padding(horizontal = 16.dp)
            .padding(bottom = 26.dp)
            .verticalScroll(state = listState)
    ) {
        ShimmerImage(
            imageUrl = eventItem.thumbnail,
            placeholderResId = R.drawable.image_not_available,
            border = Border(
                width = 1.dp,
                color = Color.LightGray,
                shape = RoundedCornerShape(8.dp)
            ),
            modifier = Modifier
                .aspectRatio(ratio = 1f)
                .fillMaxWidth()
                .clip(RoundedCornerShape(8.dp)),
        )
        Row(
            modifier = Modifier
                .padding(top = 10.dp, bottom = 10.dp)
                .fillMaxWidth(),
            verticalAlignment = Alignment.CenterVertically
        ) {
            CompositionLocalProvider(value = LocalRippleTheme provides NoRippleTheme) {
                ShimmerImage(
                    imageUrl = eventItem.thumbnail,
                    placeholderResId = R.drawable.image_not_available,
                    border = Border(
                        width = 1.dp,
                        color = Color.LightGray,
                        shape = RoundedCornerShape(percent = 50)
                    ),
                    modifier = Modifier
                        .size(50.dp)
                        .clip(CircleShape),
                )
            }
            Column(modifier = Modifier.padding(start = 12.dp)) {
                Text(text = "Dibuat Oleh", fontSize = 12.sp, fontWeight = FontWeight.Bold)
                val hostName = eventItem.eventHost.firstOrNull()?.name
                hostName?.let { Text(text = it, fontSize = 12.sp) }
                Text(text = "${eventItem.isSubscribeTotal} subscriber", fontSize = 11.sp)
            }
            Spacer(Modifier.weight(1f))
            Button(
                modifier = Modifier.height(35.dp),
                onClick = { /*TODO*/ },
                colors = ButtonDefaults.buttonColor()
            ) {
                Text(text = "Subscribe", fontSize = 12.sp)
            }
        }

        Card(
            shape = RoundedCornerShape(10.dp),
            modifier = Modifier
                .fillMaxWidth()
                .padding(top = 16.dp, bottom = 16.dp)
        ) {
            Row(
                modifier = Modifier
                    .fillMaxWidth()
                    .height(35.dp)
                    .padding(6.dp),
                horizontalArrangement = Arrangement.SpaceBetween,
                verticalAlignment = Alignment.CenterVertically
            ) {
                Text(
                    modifier = Modifier.padding(start = 16.dp),
                    text = "Manage Access",
                    fontSize = 12.sp,
                    fontWeight = FontWeight.Bold
                )
                IconButton(
                    onClick = { /*TODO*/ }
                ) {
                    Icon(Icons.AutoMirrored.Filled.ArrowForward, contentDescription = null)
                }
            }
        }


        eventItem.title?.let {
            Text(
                text = it,
                fontSize = 26.sp,
                fontWeight = FontWeight.Bold,
                overflow = TextOverflow.Ellipsis,
                maxLines = 2
            )
        }
        Row(
            modifier = Modifier
                .fillMaxWidth(),
            verticalAlignment = Alignment.CenterVertically
        ) {
            BadgeBoxIcon(
                badge = {
                    Icon(
                        modifier = Modifier
                            .size(35.dp)
                            .background(
                                color = Color.White,
                                shape = CircleShape
                            )
                            .clip(CircleShape)
                            .clickable { }
                            .padding(6.dp),
                        imageVector = Icons.Rounded.DateRange,
                        contentDescription = ""
                    )

                }, notification = false
            )
            Column(modifier = Modifier.padding(start = 12.dp)) {
                Text(
                    text = "${eventItem.eventDate?.start}",
                    fontSize = 12.sp,
                    fontWeight = FontWeight.Bold
                )
                Text(
                    text = "${eventItem.eventDate?.time} ${eventItem.eventDate?.timeZone}",
                    fontSize = 11.sp
                )
            }
        }

        Row(
            modifier = Modifier
                .fillMaxWidth(),
            verticalAlignment = Alignment.CenterVertically
        ) {
            BadgeBoxIcon(
                badge = {
                    Icon(
                        modifier = Modifier
                            .size(35.dp)
                            .background(
                                color = Color.White,
                                shape = CircleShape
                            )
                            .clip(CircleShape)
                            .clickable { }
                            .padding(6.dp),
                        imageVector = Icons.Rounded.LocationOn,
                        contentDescription = ""
                    )

                }, notification = false
            )
            Column(modifier = Modifier.padding(start = 12.dp)) {
                Text(
                    text = "${eventItem.eventLocation?.address}",
                    fontSize = 12.sp,
                    fontWeight = FontWeight.Bold
                )
                Text(text = "${eventItem.eventLocation?.addressFull}", fontSize = 11.sp)
            }
        }


        Card(
            modifier = Modifier.padding(bottom = 10.dp, top = 10.dp),
            shape = RoundedCornerShape(10.dp),
            colors = CardDefaults.cardColors(containerColor = fourColor),
            elevation = CardDefaults.cardElevation(defaultElevation = 2.dp)
        ) {
            Text(
                text = "List Ticket",
                fontSize = 14.sp,
                fontWeight = FontWeight.Bold,
                modifier = Modifier
                    .padding(bottom = 10.dp, top = 10.dp, start = 16.dp)
            )

            LazyVerticalGrid(
                modifier = Modifier.height(135.dp),
                columns = GridCells.Fixed(count = 1),
                content = {
                    items(eventItem.eventPrice.size) { index ->
                        val ticket = eventItem.eventPrice[index]
                        ListTicket(
                            ticket = ticket,
                            selected = index == selectedTicketIndex.intValue,
                            onItemClicked = {
                                selectedTicketIndex.intValue = index
                            }
                        )

                    }
                }
            )

            Button(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(bottom = 10.dp, top = 10.dp, start = 16.dp, end = 16.dp),
                onClick = {
                    navController.navigate(route = Screen.CheckIn.route)
                },
                colors = ButtonDefaults.buttonColor()
            ) {
                Text(text = "Join", fontSize = 12.sp)
            }
        }

        Text(
            modifier = Modifier.padding(top = 10.dp),
            text = "About Event",
            fontSize = 14.sp,
            fontWeight = FontWeight.Bold,
        )

        Text(
            text = "${eventItem.description}",
            fontSize = 12.sp,
            modifier = Modifier
                .padding(bottom = 10.dp, top = 5.dp)
        )

        Text(
            modifier = Modifier.padding(top = 10.dp),
            text = "Location",
            fontSize = 14.sp,
            fontWeight = FontWeight.Bold,
        )

        Card(
            modifier = Modifier
                .padding(bottom = 10.dp, top = 10.dp)
                .height(200.dp)
                .fillMaxWidth(),
            shape = RoundedCornerShape(10.dp),
            colors = CardDefaults.cardColors(containerColor = fourColor),
            elevation = CardDefaults.cardElevation(defaultElevation = 2.dp)
        ) {
            val lat = eventItem.eventLocation?.lat ?: 0.0
            val lng = eventItem.eventLocation?.lng ?: 0.0
            val googleMapsUrl =
                "https://www.google.com/maps/search/?api=1&query=${selfLocationPoint.latitude()},${selfLocationPoint.longitude()}"
            MapboxMap(
                modifier = Modifier.fillMaxSize(),
                mapViewportState = MapViewportState().apply {
                    setCameraOptions {
                        zoom(9.0)
                        center(Point.fromLngLat(107.613144, -6.905977))
                        pitch(0.0)
                        bearing(0.0)
                    }
                },
            ) {
                ViewAnnotation(
                    options = viewAnnotationOptions {
                        geometry(selfLocationPoint)
                        allowOverlap(true)
                    },
                ) {
                    Box(
                        contentAlignment = Alignment.Center,
                        modifier = Modifier
                            .size(30.dp)
                            .clickable {
                                val intent = Intent(Intent.ACTION_VIEW, Uri.parse(googleMapsUrl))
                                context.startActivity(intent)
                            }
                    ) {
                        AsyncImage(
                            model = eventItem.thumbnail,
                            contentDescription = "",
                            contentScale = ContentScale.Crop,
                            modifier = Modifier
                                .size(30.dp)
                                .clip(CircleShape)
                                .border(
                                    border = BorderStroke(width = 1.dp, color = Color.Red),
                                    shape = CircleShape
                                )
                        )
                    }
                }
            }

        }
    }

}

@Composable
fun ListTicket(ticket: EventPrice, onItemClicked: () -> Unit, selected: Boolean) {
    Card(
        modifier = Modifier
            .padding(start = 16.dp, end = 16.dp, bottom = 10.dp)
            .border(
                width = 0.4.dp,
                color = if (!selected) Color.Transparent else primaryColor,
                shape = RoundedCornerShape(10.dp)
            )
            .clickable(onClick = onItemClicked),
        shape = RoundedCornerShape(10.dp),
        elevation = CardDefaults.cardElevation(defaultElevation = 2.dp)
    ) {
        Column(
            horizontalAlignment = Alignment.CenterHorizontally,
            modifier = Modifier
                .padding(vertical = 8.dp)
        ) {
            Row(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(horizontal = 16.dp),
                verticalAlignment = Alignment.Top,
                horizontalArrangement = Arrangement.SpaceBetween
            ) {
                Text(
                    maxLines = 1,
                    overflow = TextOverflow.Ellipsis,
                    text = "${ticket.label}",
                    fontWeight = FontWeight.Bold,
                    fontSize = 12.sp
                )
                Text(
                    maxLines = 1,
                    overflow = TextOverflow.Ellipsis,
                    text = "${ticket.currency}",
                    fontWeight = FontWeight.Bold,
                    fontSize = 11.sp
                )
            }
            Row(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(horizontal = 16.dp),
                verticalAlignment = Alignment.Top,
                horizontalArrangement = Arrangement.SpaceBetween
            ) {
                Text(
                    maxLines = 1,
                    overflow = TextOverflow.Ellipsis,
                    text = "${ticket.description}",
                    fontSize = 11.sp
                )
                Text(
                    maxLines = 1,
                    overflow = TextOverflow.Ellipsis,
                    text = "${ticket.fee}",
                    fontWeight = FontWeight.Bold,
                    fontSize = 11.sp
                )
            }
        }
    }
}
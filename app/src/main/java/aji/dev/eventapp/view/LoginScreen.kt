package aji.dev.eventapp.view

import aji.dev.eventapp.R
import aji.dev.eventapp.model.UserData
import aji.dev.eventapp.navigation.Screen
import aji.dev.eventapp.network.request.LoginRequest
import aji.dev.eventapp.repository.DataStoreRepository
import aji.dev.eventapp.ui.theme.primaryColor
import aji.dev.eventapp.ui.theme.secondaryColor
import aji.dev.eventapp.util.CustomAlertDialog
import aji.dev.eventapp.util.DefaultSpacer
import aji.dev.eventapp.util.GoogleAuthUiClient
import aji.dev.eventapp.util.InputBoxShape
import aji.dev.eventapp.util.KeyboardAware
import aji.dev.eventapp.util.Loading
import aji.dev.eventapp.util.buttonColor
import aji.dev.eventapp.util.buttonModifier
import aji.dev.eventapp.util.textFieldColor
import aji.dev.eventapp.util.textFieldModifier
import aji.dev.eventapp.viewmodel.AuthViewModel
import aji.dev.eventapp.viewmodel.SignInViewModel
import android.app.Activity.RESULT_OK
import android.widget.Toast
import androidx.activity.OnBackPressedCallback
import androidx.activity.compose.rememberLauncherForActivityResult
import androidx.activity.result.IntentSenderRequest
import androidx.activity.result.contract.ActivityResultContracts
import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.Image
import androidx.compose.foundation.gestures.scrollBy
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.WindowInsets
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.ime
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.text.ClickableText
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.OutlinedButton
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.material3.TextFieldDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.DisposableEffect
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.buildAnnotatedString
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.input.PasswordVisualTransformation
import androidx.compose.ui.text.input.VisualTransformation
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.compose.ui.window.Dialog
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavHostController
import androidx.navigation.compose.currentBackStackEntryAsState
import com.google.android.gms.auth.api.identity.Identity
import kotlinx.coroutines.launch

@Composable
fun LoginScreen(
    navController: NavHostController,
    authViewModel: AuthViewModel = hiltViewModel()
) {
    val context = LocalContext.current

    val googleAuthUiClient by lazy {
        GoogleAuthUiClient(
            context = context,
            oneTapClient = Identity.getSignInClient(context)
        )
    }
    var isPasswordOpen by remember { mutableStateOf(value = false) }
    val keyboardHeight = WindowInsets.ime.getBottom(LocalDensity.current)
    val (loginData, setLoginData) = remember { mutableStateOf(LoginRequest()) }
    val scope = rememberCoroutineScope()
    val scrollState = rememberScrollState()
    val stateAuth by authViewModel.state.collectAsState()
    var showDialog by remember { mutableStateOf(value = false) }
    val shouldShowDialog = remember { mutableStateOf(value = false) }
    val dataStore = DataStoreRepository(context = context)
    val currentBackStackEntry by navController.currentBackStackEntryAsState()
    if (showDialog) Loading()
    val onBackPressedCallback = remember {
        object : OnBackPressedCallback(enabled = true), LifecycleObserver {
            override fun handleOnBackPressed() {
                navController.popBackStack(destinationId = currentBackStackEntry?.destination?.id ?: 0, inclusive = false)
            }
        }
    }
    val viewModel = viewModel<SignInViewModel>()
    val state by viewModel.state.collectAsStateWithLifecycle()
    val launcher = rememberLauncherForActivityResult(
        contract = ActivityResultContracts.StartIntentSenderForResult(),
        onResult = { result ->
            if(result.resultCode == RESULT_OK) {
                scope.launch {
                    val signInResult = googleAuthUiClient.signInWithIntent(
                        intent = result.data ?: return@launch
                    )
                    viewModel.onSignInResult(signInResult)
                }
            }
        }
    )
    DisposableEffect(key1 = currentBackStackEntry) {
        currentBackStackEntry?.lifecycle?.addObserver(onBackPressedCallback)
        onDispose {
            onBackPressedCallback.remove()
        }
    }

    LaunchedEffect(key1 = state.isSignInSuccessful) {
        if(state.isSignInSuccessful) {
            val signedInUser = googleAuthUiClient.getSignedInUser()
            if (signedInUser != null) {
                val userData = UserData(
                    userId = signedInUser.userId,
                    username = signedInUser.username,
                    profilePictureUrl = signedInUser.profilePictureUrl
                )
                Toast.makeText(
                    context,
                    userData.toString(),
                    Toast.LENGTH_LONG
                ).show()
                navController.currentBackStackEntry?.savedStateHandle?.set(
                    key = "userData",
                    value = userData
                )
                navController.navigate(route = Screen.Profile.route)
            }
        }
    }

    LaunchedEffect(key1 = keyboardHeight) {
        scope.launch {
            scrollState.scrollBy(keyboardHeight.toFloat())
        }
    }
    if (shouldShowDialog.value) {
        Dialog(onDismissRequest = { shouldShowDialog.value = false }) {
            CustomAlertDialog(
                openDialogCustom = shouldShowDialog,
                message = stateAuth.data?.results?.message.toString()
            )
        }
    }
    KeyboardAware {
        Column(
            modifier = Modifier.fillMaxSize(),
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Column(
                modifier = Modifier
                    .weight(1f)
                    .verticalScroll(state = scrollState),
            ) {
                Text(
                    text = "Login",
                    color = primaryColor,
                    modifier = Modifier.padding(top = 16.dp, start = 20.dp),
                    fontSize = 16.sp,
                    fontWeight = FontWeight.Bold
                )

                Text(
                    text = "Login to see your account",
                    color = secondaryColor,
                    modifier = Modifier.padding(top = 10.dp, start = 20.dp),
                    fontSize = 14.sp,
                )

                TextField(
                    value = loginData.email_phone,
                    onValueChange = { setLoginData(loginData.copy(email_phone = it)) },
                    modifier = Modifier.textFieldModifier(),
                    colors = TextFieldDefaults.textFieldColor(),
                    shape = InputBoxShape.medium,
                    singleLine = true,
                    leadingIcon = {
                        Row(
                            modifier = Modifier.padding(start = 8.dp),
                            verticalAlignment = Alignment.CenterVertically
                        ) {
                            Icon(
                                painter = painterResource(id = R.drawable.ic_email),
                                contentDescription = "",
                                tint = primaryColor,
                                modifier = Modifier.size(20.dp)
                            )
                            DefaultSpacer()
                        }
                    },
                    placeholder = {
                        Text(text = "Email Address", color = secondaryColor)
                    },
                    textStyle = TextStyle(
                        fontSize = 14.sp,
                        fontWeight = FontWeight.SemiBold,
                    )
                )

                TextField(
                    value = loginData.password,
                    onValueChange = { setLoginData(loginData.copy(password = it)) },
                    modifier = Modifier.textFieldModifier(),
                    colors = TextFieldDefaults.textFieldColor(),
                    keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Password),
                    shape = InputBoxShape.medium,
                    singleLine = true,
                    visualTransformation = if (!isPasswordOpen) PasswordVisualTransformation() else VisualTransformation.None,
                    leadingIcon = {
                        Row(
                            modifier = Modifier.padding(start = 8.dp),
                            verticalAlignment = Alignment.CenterVertically
                        ) {
                            Icon(
                                painter = painterResource(id = R.drawable.ic_password),
                                contentDescription = "",
                                tint = primaryColor,
                                modifier = Modifier.size(20.dp)
                            )
                            DefaultSpacer()
                        }
                    },
                    trailingIcon = {
                        IconButton(onClick = { isPasswordOpen = !isPasswordOpen }) {
                            val eyeIcon =
                                if (isPasswordOpen) R.drawable.ic_eye_close else R.drawable.ic_eye_open
                            Icon(
                                painter = painterResource(id = eyeIcon),
                                contentDescription = "",
                                tint = primaryColor,
                                modifier = Modifier.size(24.dp)
                            )
                        }
                    },
                    placeholder = {
                        Text(text = "Password", color = secondaryColor)
                    },
                    textStyle = TextStyle(
                        fontSize = 14.sp,
                        fontWeight = FontWeight.SemiBold
                    )
                )
            }
            Button(
                onClick = {
                    showDialog = true
                    scope.launch {
                        authViewModel.getLogin(
                            username = loginData.email_phone,
                            password = loginData.password
                        )

                        stateAuth.data?.results?.token?.let {
                            dataStore.saveTokenAndId(
                                token = it
                            )
                        }

                        if (stateAuth.data?.status?.code == 200) {
                            val currentEntry = currentBackStackEntry
                            navController.navigate(Screen.Create.route) {
                                popUpTo(currentEntry?.destination?.id ?: 0) {
                                    inclusive = true
                                }
                            }
                        } else {
                            shouldShowDialog.value = true
                        }
                        showDialog = false
                    }
                },
                modifier = Modifier
                    .buttonModifier()
                    .padding(bottom = 10.dp),
                colors = ButtonDefaults.buttonColor(),
                shape = InputBoxShape.medium,
                contentPadding = PaddingValues(vertical = 14.dp)
            ) {
                Text(text = "Log In")
            }
            OutlinedButton(
                onClick = {
                    scope.launch {
                        val signInIntentSender = googleAuthUiClient.signIn()
                        launcher.launch(
                            IntentSenderRequest.Builder(
                                signInIntentSender ?: return@launch
                            ).build()
                        )
                        if(state.isSignInSuccessful) {
                            val signedInUser = googleAuthUiClient.getSignedInUser()
                            if (signedInUser != null) {
                                val userData = UserData(
                                    userId = signedInUser.userId,
                                    username = signedInUser.username,
                                    profilePictureUrl = signedInUser.profilePictureUrl
                                )
                                Toast.makeText(
                                    context,
                                    userData.toString(),
                                    Toast.LENGTH_LONG
                                ).show()
                                navController.navigate(route = "${Screen.Profile.route}/${userData}")
                            }
                        }
                    }
                },
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(bottom = 10.dp)
                    .padding(horizontal = 20.dp),
                border = BorderStroke(1.dp, primaryColor),
                colors = ButtonDefaults.outlinedButtonColors(
                    contentColor = primaryColor,
                ),
                shape = InputBoxShape.medium,
                contentPadding = PaddingValues(vertical = 14.dp)
            ) {
                Image(
                    modifier = Modifier.height(20.dp),
                    painter = painterResource(id = R.drawable.ic_logo_google),
                    contentDescription = ""
                )
                Spacer(modifier = Modifier.width(4.dp))
                Text(text = "Sign in with Google", color = primaryColor)
            }
            val signInText = "Don't have an account? Sign In"
            val signInWord = "Sign In"
            val signInAnnotatedString = buildAnnotatedString {
                append(signInText)
                addStyle(
                    style = SpanStyle(
                        color = secondaryColor,
                    ),
                    start = 0,
                    end = signInText.length
                )
                addStyle(
                    style = SpanStyle(
                        color = primaryColor,
                        fontSize = 14.sp,
                        fontWeight = FontWeight.Bold,
                    ),
                    start = signInText.indexOf(signInWord),
                    end = signInText.length
                )
            }

            ClickableText(
                text = signInAnnotatedString,
                style = TextStyle(
                    textAlign = TextAlign.Center
                ),
                onClick = { offset ->
                    if (offset in signInText.indexOf(signInWord) until signInText.indexOf(signInWord) + signInWord.length) {
                        navController.navigate(route = Screen.Register.route)
                    }
                },
            )

            Spacer(modifier = Modifier.height(10.dp))

        }
    }
}

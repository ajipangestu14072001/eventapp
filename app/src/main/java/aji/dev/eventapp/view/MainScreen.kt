package aji.dev.eventapp.view

import aji.dev.eventapp.navigation.Screen
import aji.dev.eventapp.repository.DataStoreRepository
import aji.dev.eventapp.ui.theme.primaryColor
import aji.dev.eventapp.util.BadgeBoxIcon
import aji.dev.eventapp.util.Chip
import aji.dev.eventapp.util.Constant.eventOptions
import aji.dev.eventapp.util.Constant.images
import aji.dev.eventapp.util.EventItemList
import aji.dev.eventapp.util.ImageSlider
import aji.dev.eventapp.util.ListEvent
import aji.dev.eventapp.util.Loading
import aji.dev.eventapp.util.SearchBar
import aji.dev.eventapp.util.buttonColor
import aji.dev.eventapp.viewmodel.EventViewModel
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.gestures.scrollBy
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.WindowInsets
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.ime
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.foundation.pager.rememberPagerState
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.outlined.Face
import androidx.compose.material.icons.outlined.Notifications
import androidx.compose.material.icons.rounded.Create
import androidx.compose.material3.BottomSheetDefaults
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.Card
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.FloatingActionButton
import androidx.compose.material3.Icon
import androidx.compose.material3.ModalBottomSheet
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.material3.TopAppBarDefaults.topAppBarColors
import androidx.compose.material3.rememberModalBottomSheetState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.runtime.mutableIntStateOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color.Companion.White
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavHostController
import kotlinx.coroutines.launch

@OptIn(ExperimentalMaterial3Api::class, ExperimentalFoundationApi::class)
@Composable
fun MainScreen(
    navController: NavHostController,
    eventViewModel: EventViewModel = hiltViewModel(),
) {
    var openBottomSheet by rememberSaveable { mutableStateOf(false) }
    val edgeToEdgeEnabled by remember { mutableStateOf(false) }
    val bottomSheetState = rememberModalBottomSheetState(
        skipPartiallyExpanded = true
    )
    val listState = rememberLazyListState()
    val selectedOptionState = remember { mutableStateOf(value = eventOptions[0]) }
    val context = LocalContext.current
    val dataStore = DataStoreRepository(context)
    var selectedTabIndex by remember { mutableIntStateOf(0) }
    val tabItems = listOf("Login", "Register")
    val pagerState = rememberPagerState {
        tabItems.size
    }
    val keyboardHeight = WindowInsets.ime.getBottom(LocalDensity.current)
    val token = dataStore.getToken.collectAsState(initial = "")
    val scrollState = rememberScrollState()
    val scope = rememberCoroutineScope()
    LaunchedEffect(selectedTabIndex) {
        pagerState.animateScrollToPage(selectedTabIndex)
    }
    LaunchedEffect(pagerState.currentPage) {
        selectedTabIndex = pagerState.currentPage
    }
    LaunchedEffect(key1 = keyboardHeight) {
        scope.launch {
            scrollState.scrollBy(keyboardHeight.toFloat())
        }
    }
    val eventData by eventViewModel.apiEventItems.observeAsState(emptyList())
    val eventDetail by eventViewModel.apiEvenDetail.observeAsState(initial = null)
    val state by eventViewModel.state.collectAsState()
    LaunchedEffect(Unit) {
        eventViewModel.getEvent(token = "")
    }
    if (state.isLoading) Loading()
    if (openBottomSheet) {
        val windowInsets = if (edgeToEdgeEnabled)
            WindowInsets(left = 0) else BottomSheetDefaults.windowInsets

        ModalBottomSheet(
            onDismissRequest = { openBottomSheet = false },
            sheetState = bottomSheetState,
            windowInsets = windowInsets
        ) {
            eventDetail?.let { DetailScreen(eventItem = it, navController = navController) }
//            Text(text = eventDetail?.description ?: "No description available")
        }
    }
    Scaffold(
        topBar = {
            TopAppBar(
                title = {
                    Column {
                        Text(
                            text = "Your Location".uppercase(),
                            color = White,
                            fontSize = 12.sp
                        )
                        Spacer(modifier = Modifier.height(2.dp))
                        Text(
                            text = "Bukopin MT Haryono",
                            color = White,
                            fontSize = 16.sp,
                            fontWeight = FontWeight.Bold
                        )
                    }
                },
                colors = topAppBarColors(
                    containerColor = primaryColor,
                ),
                actions = {
                    BadgeBoxIcon(
                        badge = {
                            Icon(
                                modifier = Modifier
                                    .size(35.dp)
                                    .background(
                                        color = White,
                                        shape = CircleShape
                                    )
                                    .clip(CircleShape)
                                    .clickable { }
                                    .padding(6.dp),
                                imageVector = Icons.Outlined.Face,
                                contentDescription = ""
                            )

                        }, notification = false
                    )

                    BadgeBoxIcon(
                        badge = {
                            Icon(
                                modifier = Modifier
                                    .size(35.dp)
                                    .background(
                                        color = White,
                                        shape = CircleShape
                                    )
                                    .clip(CircleShape)
                                    .clickable { }
                                    .padding(6.dp),
                                imageVector = Icons.Outlined.Notifications,
                                contentDescription = ""
                            )

                        }, notification = true,
                        notificationCount = 3
                    )
                }
            )
        },
        content = {
            Column(
                modifier = Modifier
                    .fillMaxSize()
                    .padding(paddingValues = it)
            ) {
                SearchBar(
                    autoFocus = false,
                    onSearch = {
                    }
                )
                LazyRow(
                    modifier = Modifier
                        .padding(horizontal = 10.dp)
                        .fillMaxWidth()
                ) {
                    items(eventOptions) { option ->
                        Chip(
                            deliveryOptions = option.name,
                            selected = option == selectedOptionState.value,
                            onClick = {
                                selectedOptionState.value = option
                            },
                            icon = option.icon
                        )
                    }
                }
                LazyColumn(
                    state = listState,
                ) {
                    item {
                        Text(
                            text = "Event Untukmu",
                            fontSize = 14.sp,
                            fontWeight = FontWeight.Bold,
                            modifier = Modifier
                                .padding(start = 16.dp, bottom = 10.dp, top = 10.dp)
                        )
                        ImageSlider(images = images)

                        Card(
                            modifier = Modifier
                                .fillMaxWidth()
                                .padding(all = 16.dp)
                        ) {
                            Row(
                                modifier = Modifier
                                    .fillMaxWidth()
                                    .padding(6.dp),
                                horizontalArrangement = Arrangement.SpaceBetween,
                                verticalAlignment = Alignment.CenterVertically
                            ) {
                                Text(
                                    text = "There are 358 Event rewards waiting.",
                                    fontSize = 12.sp,
                                    fontWeight = FontWeight.Bold
                                )
                                Button(
                                    modifier = Modifier.height(35.dp),
                                    onClick = { /*TODO*/ },
                                    colors = ButtonDefaults.buttonColor()
                                ) {
                                    Text(text = "View", fontSize = 12.sp)
                                }
                            }
                        }
                    }

                    item {
                        Text(
                            text = "Bergabung Lagi",
                            fontSize = 14.sp,
                            fontWeight = FontWeight.Bold,
                            modifier = Modifier
                                .padding(start = 16.dp, bottom = 2.dp)
                        )

                        EventItemList(eventItems = eventData) { code ->
                            openBottomSheet = !openBottomSheet
                            scope.launch {
                                eventViewModel.showEvent(token = "", code = code)
                            }
                        }


                        Text(
                            text = "Event Sekitar Lokasimu",
                            fontSize = 14.sp,
                            fontWeight = FontWeight.Bold,
                            modifier = Modifier
                                .padding(start = 16.dp, bottom = 10.dp, top = 16.dp)
                        )

                        ImageSlider(images = images)
                    }

                    item {
                        Text(
                            text = "Event Terdekat",
                            fontSize = 14.sp,
                            fontWeight = FontWeight.Bold,
                            modifier = Modifier
                                .padding(start = 16.dp, bottom = 10.dp, top = 16.dp)
                        )
                    }
                    items(eventData) { data ->
                        ListEvent(eventItem = data){code ->
                            openBottomSheet = !openBottomSheet
                            scope.launch {
                                eventViewModel.showEvent(token = "", code = code)
                            }
                        }
                    }
                }
            }
        },
        floatingActionButton = {
            FloatingActionButton(
                onClick = {
                    scope.launch {
                        if (token.value?.isNotEmpty() == true) {
                            navController.navigate(Screen.Create.route)
                        } else{
                            navController.navigate(Screen.Login.route)
                        }
                    }
                },
            ) {
                Icon(Icons.Rounded.Create, contentDescription = null)
            }
        }
    )
}
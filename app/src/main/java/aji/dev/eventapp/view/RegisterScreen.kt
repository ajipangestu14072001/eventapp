package aji.dev.eventapp.view

import aji.dev.eventapp.R
import aji.dev.eventapp.navigation.Screen
import aji.dev.eventapp.network.request.LoginRequest
import aji.dev.eventapp.network.request.RegisterRequest
import aji.dev.eventapp.repository.DataStoreRepository
import aji.dev.eventapp.ui.theme.primaryColor
import aji.dev.eventapp.ui.theme.secondaryColor
import aji.dev.eventapp.util.CustomAlertDialog
import aji.dev.eventapp.util.DefaultSpacer
import aji.dev.eventapp.util.InputBoxShape
import aji.dev.eventapp.util.KeyboardAware
import aji.dev.eventapp.util.Loading
import aji.dev.eventapp.util.buttonColor
import aji.dev.eventapp.util.buttonModifier
import aji.dev.eventapp.util.textFieldColor
import aji.dev.eventapp.util.textFieldModifier
import aji.dev.eventapp.viewmodel.AuthViewModel
import androidx.activity.OnBackPressedCallback
import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.Image
import androidx.compose.foundation.gestures.scrollBy
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.WindowInsets
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.ime
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.text.ClickableText
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.rounded.Create
import androidx.compose.material.icons.rounded.Phone
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.OutlinedButton
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.material3.TextFieldDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.DisposableEffect
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.buildAnnotatedString
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.input.PasswordVisualTransformation
import androidx.compose.ui.text.input.VisualTransformation
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.compose.ui.window.Dialog
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.lifecycle.LifecycleObserver
import androidx.navigation.NavHostController
import androidx.navigation.compose.currentBackStackEntryAsState
import kotlinx.coroutines.launch

@Composable
fun RegisterScreen(
    navController: NavHostController,
    authViewModel: AuthViewModel = hiltViewModel()
) {
    var isPasswordOpen by remember { mutableStateOf(value = false) }
    val scroll = rememberScrollState()
    val keyboardHeight = WindowInsets.ime.getBottom(LocalDensity.current)
    val scope = rememberCoroutineScope()
    var showDialog by remember { mutableStateOf(value = false) }
    val stateAuth by authViewModel.state.collectAsState()
    val shouldShowDialog = remember { mutableStateOf(value = false) }
    val currentBackStackEntry by navController.currentBackStackEntryAsState()
    val (registerData, setRegisData) = remember { mutableStateOf(RegisterRequest()) }
    val context = LocalContext.current
    val dataStore = DataStoreRepository(context = context)
    LaunchedEffect(key1 = keyboardHeight) {
        scope.launch {
            scroll.scrollBy(keyboardHeight.toFloat())
        }
    }
    if (showDialog) Loading()
    val onBackPressedCallback = remember {
        object : OnBackPressedCallback(enabled = true), LifecycleObserver {
            override fun handleOnBackPressed() {
                navController.popBackStack(destinationId = currentBackStackEntry?.destination?.id ?: 0, inclusive = false)
            }
        }
    }
    DisposableEffect(key1 = currentBackStackEntry) {
        currentBackStackEntry?.lifecycle?.addObserver(onBackPressedCallback)
        onDispose {
            onBackPressedCallback.remove()
        }
    }
    if (shouldShowDialog.value) {
        Dialog(onDismissRequest = { shouldShowDialog.value = false }) {
            CustomAlertDialog(
                openDialogCustom = shouldShowDialog,
                message = stateAuth.data?.results?.message.toString()
            )
        }
    }
    KeyboardAware {
        Column(
            modifier = Modifier.fillMaxSize(),
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Column(
                modifier = Modifier
                    .weight(1f)
                    .verticalScroll(state = scroll),
            ) {
                Text(
                    text = "Register",
                    color = primaryColor,
                    modifier = Modifier.padding(top = 16.dp, start = 20.dp),
                    fontSize = 16.sp,
                    fontWeight = FontWeight.Bold
                )

                Text(
                    text = "Register to create your account",
                    color = secondaryColor,
                    modifier = Modifier.padding(top = 10.dp, start = 20.dp),
                    fontSize = 14.sp,
                )
                TextField(
                    value = registerData.name,
                    onValueChange = { setRegisData(registerData.copy(name = it)) },
                    modifier = Modifier.textFieldModifier(),
                    colors = TextFieldDefaults.textFieldColor(),
                    shape = InputBoxShape.medium,
                    singleLine = true,
                    leadingIcon = {
                        Row(
                            modifier = Modifier.padding(start = 8.dp),
                            verticalAlignment = Alignment.CenterVertically
                        ) {
                            Icon(
                                imageVector = Icons.Rounded.Create,
                                contentDescription = "",
                                tint = primaryColor,
                                modifier = Modifier.size(20.dp)
                            )
                            DefaultSpacer()
                        }
                    },
                    placeholder = {
                        Text(text = "Full Name", color = secondaryColor)
                    },
                    textStyle = TextStyle(
                        fontSize = 14.sp,
                        fontWeight = FontWeight.SemiBold,
                    )
                )

                TextField(
                    value = registerData.email,
                    onValueChange = { setRegisData(registerData.copy(email = it)) },
                    modifier = Modifier.textFieldModifier(),
                    colors = TextFieldDefaults.textFieldColor(),
                    shape = InputBoxShape.medium,
                    singleLine = true,
                    leadingIcon = {
                        Row(
                            modifier = Modifier.padding(start = 8.dp),
                            verticalAlignment = Alignment.CenterVertically
                        ) {
                            Icon(
                                painter = painterResource(id = R.drawable.ic_email),
                                contentDescription = "",
                                tint = primaryColor,
                                modifier = Modifier.size(20.dp)
                            )
                            DefaultSpacer()
                        }
                    },
                    placeholder = {
                        Text(text = "Email Address", color = secondaryColor)
                    },
                    textStyle = TextStyle(
                        fontSize = 14.sp,
                        fontWeight = FontWeight.SemiBold,
                    )
                )
                TextField(
                    value = registerData.phone,
                    onValueChange = { setRegisData(registerData.copy(phone = it)) },
                    modifier = Modifier.textFieldModifier(),
                    colors = TextFieldDefaults.textFieldColor(),
                    shape = InputBoxShape.medium,
                    singleLine = true,
                    leadingIcon = {
                        Row(
                            modifier = Modifier.padding(start = 8.dp),
                            verticalAlignment = Alignment.CenterVertically
                        ) {
                            Icon(
                                imageVector = Icons.Rounded.Phone,
                                contentDescription = "",
                                tint = primaryColor,
                                modifier = Modifier.size(20.dp)
                            )
                            DefaultSpacer()
                        }
                    },
                    placeholder = {
                        Text(text = "Phone Number", color = secondaryColor)
                    },
                    textStyle = TextStyle(
                        fontSize = 14.sp,
                        fontWeight = FontWeight.SemiBold,
                    )
                )

                TextField(
                    value = registerData.password,
                    onValueChange = { setRegisData(registerData.copy(password = it)) },
                    modifier = Modifier.textFieldModifier(),
                    colors = TextFieldDefaults.textFieldColor(),
                    keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Password),
                    shape = InputBoxShape.medium,
                    singleLine = true,
                    visualTransformation = if (!isPasswordOpen) PasswordVisualTransformation() else VisualTransformation.None,
                    leadingIcon = {
                        Row(
                            modifier = Modifier.padding(start = 8.dp),
                            verticalAlignment = Alignment.CenterVertically
                        ) {
                            Icon(
                                painter = painterResource(id = R.drawable.ic_password),
                                contentDescription = "",
                                tint = primaryColor,
                                modifier = Modifier.size(20.dp)
                            )
                            DefaultSpacer()
                        }
                    },
                    trailingIcon = {
                        IconButton(onClick = { isPasswordOpen = !isPasswordOpen }) {
                            if (!isPasswordOpen) {
                                Icon(
                                    painter = painterResource(id = R.drawable.ic_eye_open),
                                    contentDescription = "",
                                    tint = primaryColor,
                                    modifier = Modifier.size(24.dp)
                                )
                            } else {
                                Icon(
                                    painter = painterResource(id = R.drawable.ic_eye_close),
                                    contentDescription = "",
                                    tint = primaryColor,
                                    modifier = Modifier.size(24.dp)
                                )
                            }
                        }
                    },
                    placeholder = {
                        Text(text = "Password", color = secondaryColor)
                    },
                    textStyle = TextStyle(
                        fontSize = 14.sp,
                        fontWeight = FontWeight.SemiBold
                    )
                )
            }
            Button(
                onClick = {
                    showDialog = true
                    scope.launch {
                        authViewModel.getRegister(
                            name = registerData.name,
                            password = registerData.password,
                            email = registerData.email,
                            phone = registerData.phone
                        )

                        stateAuth.data?.results?.token?.let {
                            dataStore.saveTokenAndId(
                                token = it
                            )
                        }

                        if (stateAuth.data?.status?.code == 200) {
                            val currentEntry = currentBackStackEntry
                            navController.navigate(Screen.Create.route) {
                                popUpTo(currentEntry?.destination?.id ?: 0) {
                                    inclusive = true
                                }
                            }
                        } else {
                            shouldShowDialog.value = true
                        }
                        showDialog = false
                    }
                },
                modifier = Modifier
                    .buttonModifier()
                    .padding(bottom = 10.dp),
                colors = ButtonDefaults.buttonColor(),
                shape = InputBoxShape.medium,
                contentPadding = PaddingValues(vertical = 14.dp)
            ) {
                Text(text = "Register")
            }
            OutlinedButton(
                onClick = {
                },
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(bottom = 10.dp)
                    .padding(horizontal = 20.dp),
                border = BorderStroke(1.dp, primaryColor),
                colors = ButtonDefaults.outlinedButtonColors(
                    contentColor = primaryColor,
                ),
                shape = InputBoxShape.medium,
                contentPadding = PaddingValues(vertical = 14.dp)
            ) {
                Image(
                    modifier = Modifier.height(20.dp),
                    painter = painterResource(id = R.drawable.ic_logo_google),
                    contentDescription = ""
                )
                Spacer(modifier = Modifier.width(4.dp))
                Text(text = "Sign in with Google", color = primaryColor)
            }
            val signInText = "Already have an account? Log In"
            val signInWord = "Log In"
            val signInAnnotatedString = buildAnnotatedString {
                append(signInText)
                addStyle(
                    style = SpanStyle(
                        color = secondaryColor,
                    ),
                    start = 0,
                    end = signInText.length
                )
                addStyle(
                    style = SpanStyle(
                        color = primaryColor,
                        fontSize = 14.sp,
                        fontWeight = FontWeight.Bold,
                    ),
                    start = signInText.indexOf(signInWord),
                    end = signInText.length
                )
            }
            ClickableText(
                text = signInAnnotatedString,
                style = TextStyle(
                    textAlign = TextAlign.Center
                ),
                onClick = { offset ->
                    if (offset in signInText.indexOf(signInWord) until signInText.indexOf(signInWord) + signInWord.length) {
                        navController.navigate(route = Screen.Login.route)
                    }
                },
            )

            Spacer(modifier = Modifier.height(10.dp))
        }
    }
}
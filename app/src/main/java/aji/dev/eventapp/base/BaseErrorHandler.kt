package aji.dev.eventapp.base

import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject

open class BaseErrorHandler<STATE>(
    initialState: STATE,
    private val errorMessageKey: String = "Message",
    private val errorsArrayKey: String = "Errors",
    private val fieldKey: String = "Field",
    private val messageKey: String = "Message"
) : BaseViewModel<STATE>(initialState) {

    private val _errorMessage = MutableStateFlow<String?>(null)
    val errorMessage: StateFlow<String?> = _errorMessage

    protected fun parseErrorResponse(errorResponse: String?): String? {
        errorResponse?.let {
            try {
                val jsonObject = JSONObject(it)
                val errorMessage = if (jsonObject.has(errorMessageKey)) {
                    jsonObject.getString(errorMessageKey)
                } else if (jsonObject.has(errorsArrayKey)) {
                    val errorsArray = jsonObject.getJSONArray(errorsArrayKey)
                    parseErrorsArray(errorsArray)
                } else {
                    null
                }
                _errorMessage.value = errorMessage
                return errorMessage
            } catch (e: JSONException) {
                e.printStackTrace()
            }
        }
        return null
    }

    private fun parseErrorsArray(errorsArray: JSONArray): String {
        val errorMessages = mutableListOf<String>()
        for (i in 0 until errorsArray.length()) {
            val errorObject = errorsArray.getJSONObject(i)
            val field = errorObject.getString(fieldKey)
            val errorMessage = errorObject.getString(messageKey)
            errorMessages.add("$field: $errorMessage")
        }
        return errorMessages.joinToString("\n")
    }
}

